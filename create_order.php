<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


require_once 'app/Mage.php';
umask(0);
Mage::app('default');

class MonogramOnlineClient
{
    private $client;
    private $sessionId;

    public function __construct()
    {
        $wsdlUrl = 'http://www.pws-services.net/api/v2_soap?wsdl=1';
        $this->client = new SoapClient($wsdlUrl);
        $this->sessionId = $this->client->login('collage', 'Collage99~!');
    }

    public function getSessionId()
    {
        if ($this->sessionId) {
            return true;
        } else {
            return false;
        }
    }
}

$checkSession = new MonogramOnlineClient();
if ($checkSession->getSessionId()) {
//    echo "SOAP OK";
} else {
    echo "SOAP Access denied.";
    exit();
}

//#################################
////foreach (Mage::getStoreConfig('carriers') as $code => $config) {
////    if (!empty($config['title'])) {
////        $carriers[] = array('value' => $code, 'label' => $config['title']);
////    }
////}
//$apiHelper = Mage::helper('oms_api/oms');
//$apiHelper->jdbg("carriers = ",Mage::getStoreConfig('carriers'));
//
//
//exit();
//
//#################################

$store = Mage::app()->getStore();
$website = Mage::app()->getWebsite();

$quote = Mage::getModel('sales/quote')->setStoreId($store->getStoreId());

### 1) Add customer email address make sure same customer exist in Magento Back office.
$customerEmail = "tarikuli@yahoo.com";


$customer = Mage::getModel('customer/customer')->setWebsiteId($website->getId())->loadByEmail($customerEmail);

if ($customer->getId()) {
    $billingAddressArray = $customer->getPrimaryBillingAddress();
    $shippingAddress = $customer->getPrimaryShippingAddress();
} else {
    echo "Client email " . $customerEmail . " not exist in back office";
    exit();
}

### 2) Add Shipping person First name.
$customerFirstname = "Shipping person First name";

### 3) Add Shipping person Last  name
$customerLastname = "Shipping person Last  name";

$billingAddress = array(
    'customer_address_id' => '',
    'prefix' => '',
    'firstname' => $customer->getFirstname(),
    'middlename' => '',
    'lastname' => $customer->getLastname(),
    'suffix' => '',
    'company' => '',
//    'street' => array(
//        '0' => $billingAddressArray->getStreet1(), // required
//        '1' => $billingAddressArray->getStreet2() // optional
//    ),
    'street' =>$billingAddressArray->getStreet1(),
    'city' => $billingAddressArray->getCity(),
    'country_id' => 'US', // country code
    'region' => $billingAddressArray->getRegion(),
    'postcode' => $billingAddressArray->getPostcode(),
    'telephone' => $billingAddressArray->getTelephone(),
    'fax' => '',
    'save_in_address_book' => 1
);

### 3) Add Shipping Address
$shippingAddress = array(
    'customer_address_id' => '',
    'prefix' => '',
    'firstname' => $customer->getFirstname(),
    'middlename' => '',
    'lastname' => $customer->getLastname(),
    'suffix' => '',
    'company' => '',
//    'street' => array(
//        '0' => $billingAddressArray->getStreet1(), // required
//        '1' => $billingAddressArray->getStreet2() // optional
//    ),
    'street' =>$billingAddressArray->getStreet1(),
    'city' => $billingAddressArray->getCity(),
    'country_id' => 'US', // country code
    'region' => $billingAddressArray->getRegion(),
//    'region_id' => '2',
    'postcode' => $billingAddressArray->getPostcode(),
    'telephone' =>  $billingAddressArray->getTelephone(),
    'fax' => '',
    'save_in_address_book' => 1
);

$quote->assignCustomer($customer);
$quote->setCurrency(Mage::app()->getStore()->getBaseCurrencyCode());

### 4) Product Id, Quantity, Image 1 Url, Image 2 Url
$productIds = array(
    8116 => [1, "image url 1", "image url 2"],
    8115 => [1, "image url 11", "image url 22"]
);

foreach ($productIds as $productId => $qty) {
    $product = Mage::getModel('catalog/product')->load($productId);
    $formKey = Mage::getSingleton('core/session')->getFormKey();
    $customOptions = $product->getOptions();
    $options = array();
    $request = new Varien_Object();
    $i = 0;

    foreach ($customOptions as $option) {
        $i++;
        $options[$option->getOptionId()] = $qty[$i];
    }

    try {
        $paramater = array(
            'product_id' => $productId,
            'qty' => $qty[0],
            'form_key' => $formKey,
            'options' => $options
        );
        $request->setData($paramater);
        $quote->addProduct($product, $request);
    } catch (exception $e) {
        var_dump($e->getMessage());
        exit();
    }
}

$billingAddressData = $quote->getBillingAddress()->addData($billingAddress);
$shippingAddressData = $quote->getShippingAddress()->addData($shippingAddress);
$shippingAddressData->setCollectShippingRates(true)->collectShippingRates();

### 5) Change shipping method like "flatrate_flatrate" "jewel_priorityshipping_priority" "jewel_expressshipping_express"
$shippingAddressData->setShippingMethod('jewel_priorityshipping_priority')->setPaymentMethod('checkmo');
$quote->getPayment()->importData(array('method' => 'checkmo'));

try {
    //Collect totals & save quote
    $quote->collectTotals();
    $quote->getShippingAddress()->collectTotals();
    $quote->save();

    //Create order from quote
    $service = Mage::getModel('sales/service_quote', $quote);
    $service->submitAll();

    $orderId = $service->getOrder()->getId();
    $increment_id = $service->getOrder()->getRealOrderId();
    $increment_id . ' has been successfully created.';

    $apiHelper = Mage::helper('oms_api/oms');

    $apiHelper->getOmsUrl();
    if (Mage::registry('oms_array')) {
        $oms = Mage::registry('oms_array');
        Mage::unregister('oms_array');

        $apiHelper->pushPurchaseToOms([$orderId], $oms['storeID'], $oms['path'], $oms['omsUrl']);
        echo 'Order Id : ' . $increment_id . ' has been successfully created.';
    } else {
        echo 'Order Id = (' . $increment_id . ') No 5p Store found. ';
        Mage::log('Order_ID = (' . $increment_id . ') No 6p Store found. ', null, 'jewel_oms.log');
    }

} catch (Exception $e) {
    Mage::logException($e);
}