<?php
require_once 'app/Mage.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// https://techbandhu.wordpress.com/2013/05/23/magento-how-to-get-all-active-payment-methods-and-shipping-methods/

class subMitOrder
{
    protected $client;

    public function __construct()
    {

        $customerInfo = new EscapedAddress(
            "Craig",
            "Schroeder",
            "1327 Jones Dr",
            "",
            "Ann Arbor",
            "MI",
            "US",
            "48103"
        );

        $this->client = new MonogramOnlineOrderSubmitUtil(
            $customerInfo,
            [
                [
                    'product_code' => 'SMB3040',
                    'quantity' => 2,
                    'images' =>
                        [
                            'https://api.collage.com/v2.php/renderondemand/14925764_8851c38d2438b5f6.jpg',
                        ],
                ],
                [
                    'product_code' => 'DMB3040',
                    'quantity' => 2,
                    'images' =>
                        [
                            'https://api.collage.com/v2.php/renderondemand/14925764_8851c38d2438b5f6.jpg',
                            'https://api.collage.com/v2.php/renderondemand/14905870_1355892e6f29eac5.jpg',
                        ],
                ],
            ],
            '1245324'
        );

    }

    public function pushOrder(){
        try {
//            var_dump($client->sendOrder());
            return $this->client->sendOrder();
        } catch (exception $e) {
            var_dump($e->getMessage());
        }
    }

}

umask(0);
Mage::app('default');

$store = Mage::app()->getStore();
//var_dump($store->getStoreId());
$website = Mage::app()->getWebsite();
//var_dump($website->getId());

$orderIds = new subMitOrder;
$orderId = $orderIds->pushOrder();
var_dump($orderId);

$apiHelper = Mage::helper('oms_api/oms');

$apiHelper->getOmsUrl();
if (Mage::registry('oms_array')) {
    $oms = Mage::registry('oms_array');
    var_dump($oms);
    Mage::unregister('oms_array');

    $apiHelper->pushPurchaseToOms([$orderId], $oms['storeID'], $oms['path'], $oms['omsUrl']);
} else {
    Mage::log('Order_ID = (' . $orderId . ') No 5p Store found. ', null, 'jewel_oms.log');
}


class EscapedAddress
{
    public $fname;
    public $lname;
    public $street1;
    public $street2;
    public $city;
    public $stateprov;
    public $country;
    public $zip;

    public function __construct(
        $fname,
        $lname,
        $street1,
        $street2,
        $city,
        $stateprov,
        $country,
        $zip
    )
    {
        $this->fname = $fname;
        $this->lname = $lname;
        $this->street1 = $street1;
        $this->street2 = $street2;
        $this->city = $city;
        $this->stateprov = $stateprov;
        $this->country = $country;
        $this->zip = $zip;
    }
}

class MonogramOnlineOrderSubmitUtil
{
    private $customerAddress;
    private $orderItems;
    private $orderId;

    public function __construct(EscapedAddress $customerAddress, $orderItems, $orderId)
    {
        $this->customerAddress = $customerAddress;
        $this->orderItems = $orderItems;
        $this->orderId = $orderId;
    }

    /**
     * Performs network operations needed to send data
     * to our print partner.
     * Matches implementation based on sendOrder
     * in SubmitOrder.php as of 8/7/2019
     * Returns the 'increment_id'/'orderIncrementId' referred to
     * in the docs: https://devdocs.magento.com/guides/m1x/api/soap/sales/salesOrder/sales_order.info.html
     * Throws exceptions from the functions it calls and doesn't
     * attempt to handle them
     */
    public function sendOrder()
    {
        $client = new MonogramOnlineClient();
        $client->createCart();
        $client->setCustomer($this->customerAddress, '888-111-2222');
        foreach ($this->orderItems as $item) {
            $client->addProductToCart($item);
        }
        $client->setShippingMethod();
        $client->setPaymentMethod($this->orderId);
        return $client->shoppingCartOrder();
    }
}


class MonogramOnlineClient
{
    const STORE_ID = '1';
    private $client;
    private $sessionId;
    private $cartId;

    public function __construct()
    {
        $wsdlFilePath = __DIR__ . '/monogram_online.wsdl';
        $wsdlUrl = 'http://www.pws-services.net/api/v2_soap?wsdl=1';
        $this->client = new SoapClient($wsdlUrl);
        $this->sessionId = $this->client->login('collage', 'Collage99~!');
    }

    public function createCart()
    {
        $this->cartId = $this->client->shoppingCartCreate($this->sessionId, self::STORE_ID);
    }

    function setCustomer(EscapedAddress $customerAddress, $ship_to_phone)
    {
        $this->client->shoppingCartCustomerAddresses($this->sessionId, $this->cartId, [
            [
                "mode" => "shipping",
                "firstname" => $customerAddress->fname,
                "lastname" => $customerAddress->lname,
                "street" => $customerAddress->street1 . "\n" . $customerAddress->street2,
                "city" => $customerAddress->city,
                "region" => $customerAddress->stateprov,
                "postcode" => $customerAddress->zip,
                "country_id" => $customerAddress->country,
                "telephone" => $ship_to_phone,
                "is_default_shipping" => 0,
                "is_default_billing" => 0,
            ],
            [
                "mode" => "billing",
                "firstname" => "COLLAGE.COM",
                "lastname" => "COLLAGE.COM",
                "street" => "123 Some Way",
                "city" => "Brighton",
                "region" => "MI",
                "postcode" => "48178",
                "country_id" => "US",
                "telephone" => $ship_to_phone,
            ]
        ], self::STORE_ID);
    }

    function setShippingMethod()
    {
        try {
            return $this->client->shoppingCartShippingMethod($this->sessionId, $this->cartId, 'flatrate_flatrate', self::STORE_ID);
        } catch (SoapFault $e) {
            var_dump("setShippingMethod " . $e->getMessage());
        }
    }

    function setPaymentMethod($collageOrderId)
    {
        try {
            return $this->client->shoppingCartPaymentMethod($this->sessionId, $this->cartId, [
                'po_number' => $collageOrderId,
                'method' => 'checkmo',
                'cc_cid' => null,
                'cc_owner' => null,
                'cc_number' => null,
                'cc_type' => null,
                'cc_exp_year' => null,
                'cc_exp_month' => null
            ], self::STORE_ID);
        } catch (SoapFault $e) {
            var_dump("setPaymentMethod " . $e->getMessage());
        }
    }

    function addProductToCart(array $item)
    {
// Magento Magic Numbers for the 'Image' and 'Backside' custom fields
        $imageUrlOptionId = [
            'SFB3040' => '41259',
            'SFB5060' => '41265',
            'SMB3040' => '41264',
            'SMB5060' => '41263',
            'DFB3040' => '41262',
            'DFB5060' => '41261',
            'DMB3040' => '41260',
            'DMB5060' => '41269',
        ];
        $backsideUrlOptionId = [
            'DFB3040' => '41266',
            'DFB5060' => '41267',
            'DMB3040' => '41268',
            'DMB5060' => '41270',
        ];
        try {
            $options = [];
            $options[] = [
                'key' => $imageUrlOptionId[$item['product_code']],
                'value' => $item['images'][0],
            ];
// We should consider if there is a more robust way to do this.
// We could do something like use productinfo to specify a
// function name that transforms items or something.
// A large number of them only need to put the images
// into an xml tag with a url, only a few would need this
// special treatment with different metadata and such
            if (isset($backsideUrlOptionId[$item['product_code']])) {
                $options[] = [
                    'key' => $backsideUrlOptionId[$item['product_code']],
                    'value' => $item['images'][1],
                ];
            }
            $this->client->shoppingCartProductAdd($this->sessionId, $this->cartId, [
                [
                    "sku" => $item['product_code'],
                    "qty" => $item['quantity'],
                    "options" => $options,
                ],
            ], self::STORE_ID);
        } catch (SoapFault $e) {
            var_dump($item['product_code'] . " " . $e->getMessage());
        }
    }

    function shoppingCartOrder()
    {
        return $this->client->shoppingCartOrder($this->sessionId, $this->cartId, self::STORE_ID);
    }
}