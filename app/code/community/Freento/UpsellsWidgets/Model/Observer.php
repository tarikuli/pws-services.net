<?php

class Freento_UpsellsWidgets_Model_Observer
{
    
    /**
     * add custom scripts to Freento_Upsells admin pages custom scripts block
     * 
     * @param Varien_Event_Observer $observer
     */
    public function addCustomScripts($observer)
    {
        $block = $observer->getBlock();
        
        $scriptsBlock = Mage::app()->getLayout()->createBlock('core/template', 'freento_upsellswidgets_scripts');
        $scriptsBlock->setTemplate('freento/upsellswidgets/scripts.phtml');
        
        $block->append($scriptsBlock, 'freento_upsellswidgets_scripts');
    }
    
}