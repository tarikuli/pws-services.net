<?php

class Freento_UpsellsWidgets_Block_Widget_Data_Form_Element_Date
  extends Mage_Adminhtml_Block_Widget_Form_Renderer_Fieldset_Element
{
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $date = new Varien_Data_Form_Element_Date($element->getData());
        $date->setId($element->getId());
        $date->setForm($element->getForm());
        $date->setFormat('y/M/d');
        $date->setImage($this->getSkinUrl('images/grid-cal.gif'));
        return parent::render($date);
    }
    
}