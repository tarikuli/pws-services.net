<?php

class Freento_UpsellsWidgets_Block_Widget extends Freento_Upsells_Block_Widget
{
    
    protected function _toHtml()
    {
        $dateFrom = $this->getDateFrom();
        $dateTo = $this->getDateTo();
        if (Mage::app()->getLocale()->isStoreDateInInterval(Mage::app()->getStore()->getId(), $dateFrom, $dateTo)) {
            return parent::_toHtml();
        }
        
        return '';
    }
    
}