<?php

$installer = $this;
$installer->startSetup();

$installer->run(
    "ALTER TABLE {$this->getTable('freento_upsells/rule')} ADD COLUMN (
        discount decimal(10, 4),
        discount_type varchar(10)
    )"
);

$installer->endSetup();