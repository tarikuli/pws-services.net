<?php

class Freento_UpsellsRules_Model_Observer_Validate
{
    
    /**
     * add custom data to rule validation object
     * 
     * @param Varien_Event_Observer $observer
     */
    public function validateBefore($observer)
    {
        
        $product = null;
        
        if ($block = $observer->getValidateObject()->getBlock()) {
            if ($productId = $block->getData('product_id')) {
                $product = Mage::getModel('catalog/product')->load($productId);
                $observer->getValidateObject()->setProduct($product);
            }
            
            if ($categoryId = $block->getData('category_id')) {
                $category = Mage::getModel('catalog/category')->load($categoryId);
                $observer->getValidateObject()->setCategory($category);
            }
        }
        
        $session = Mage::getSingleton('customer/session');  
        $customer = $session->getCustomer();
        
        if (!$session->isLoggedIn()) {
            $customer->setData('group_id', Mage_Customer_Model_Group::NOT_LOGGED_IN_ID);
        }
        
        $observer->getValidateObject()->setCustomer($customer);
    }
    
    public function validateAfter($observer)
    {
        return $observer;
    }

}