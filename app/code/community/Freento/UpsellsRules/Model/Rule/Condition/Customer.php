<?php

class Freento_UpsellsRules_Model_Rule_Condition_Customer extends Mage_Rule_Model_Condition_Abstract
{
    
    /**
     * set all customer attributes to rule conditions
     * 
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $customerAttributes = Mage::getResourceSingleton('customer/customer')
            ->loadAllAttributes()
            ->getAttributesByCode();

        $attributes = array();
        foreach ($customerAttributes as $attribute) {
            if (!$attribute->getFrontendLabel()) {
              continue;
            }
            
            $attributes[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
        }

        asort($attributes);
        $this->setAttributeOption($attributes);

        return $this;
    }
}