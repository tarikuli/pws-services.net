<?php

class Freento_UpsellsRules_Model_Rule_Condition_Category extends Mage_Rule_Model_Condition_Abstract
{
    
    /**
     * set all category attributes to rule conditions
     * 
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $categoryAttributes = Mage::getResourceSingleton('catalog/category')
            ->loadAllAttributes()
            ->getAttributesByCode();

        $attributes = array();
        foreach ($categoryAttributes as $attribute) {
            if (!$attribute->getFrontendLabel()) {
                continue;
            }
            
            $attributes[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
        }

        asort($attributes);
        $this->setAttributeOption($attributes);

        return $this;
    }
}