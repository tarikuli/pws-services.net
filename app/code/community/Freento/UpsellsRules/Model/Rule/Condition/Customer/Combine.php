<?php

class Freento_UpsellsRules_Model_Rule_Condition_Customer_Combine extends Mage_Rule_Model_Condition_Combine
{

    public function __construct()
    {
        parent::__construct();
        $this->setType('freento_upsellsrules/rule_condition_customer_combine');
    }
    
    /**
     * 
     * @return $this
     */
    public function loadValueOptions()
    {
        $this->setValueOption(
            array(
                1 => Mage::helper('salesrule')->__('TRUE'),
                0 => Mage::helper('salesrule')->__('FALSE')
            )
        );
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function asHtml()
    {
        $html = $this->getTypeElement()->getHtml();
        $html .= Mage::helper('freento_upsellsrules')->__(
            'If %s of the current customer conditions are %s:',
            $this->getAggregatorElement()->getHtml(),
            $this->getValueElement()->getHtml()
        );
        
        if ($this->getId() != '1') {
            $html .= $this->getRemoveLinkHtml();
        }
        
        return $html;
    }
    
    /**
     * 
     * @return array
     */
    public function getNewChildSelectOptions()
    {
        $customerCondition = Mage::getModel('freento_upsellsrules/rule_condition_customer');
        $customerAttributes = $customerCondition->loadAttributeOptions()->getAttributeOption();
        $cAttributes = array();
        foreach ($customerAttributes as $code => $label) {
            if (strpos($code, 'quote_item_') !== 0) {
                $cAttributes[] = array(
                    'value' => 'freento_upsellsrules/rule_condition_customer|' . $code,
                    'label' => $label
                );
            }
        }

        $conditions = parent::getNewChildSelectOptions();
        $conditions = array(
            array('value' => '', 'label' => Mage::helper('rule')->__('Please choose a condition to add...')),
            array('label' => Mage::helper('catalog')->__('Customer Attribute'), 'value' => $cAttributes),
        );
        
        return $conditions;
    }
    
    /**
     * 
     * @param Varien_Object $object
     * @return boolean
     */
    public function validate(Varien_Object $object)
    {
        if ($customer = $object->getCustomer()) {
            return parent::validate($customer);
        }

        return false;
    }

}
