<?php

class Freento_UpsellsRules_Model_Rule_Condition_Product extends Mage_CatalogRule_Model_Rule_Condition_Product
{
    
    /**
     * set all product attributes to rule conditions
     * 
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $productAttributes = Mage::getResourceSingleton('catalog/product')
            ->loadAllAttributes()
            ->getAttributesByCode();

        $attributes = array();
        foreach ($productAttributes as $attribute) {
            if (!$attribute->getFrontendLabel()) {
                continue;
            }

            $attributes[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
        }
        
        $this->_addSpecialAttributes($attributes);

        asort($attributes);
        $this->setAttributeOption($attributes);

        return $this;
    }
}
