<?php

class Freento_UpsellsRules_Model_Rule_Condition_Product_Page_Combine extends Mage_Rule_Model_Condition_Combine
{
    
    public function __construct()
    {
        parent::__construct();
        $this->setType('freento_upsellsrules/rule_condition_product_page_combine');
    }
    
    /**
     * 
     * @return $this
     */
    public function loadValueOptions()
    {
        $this->setValueOption(
            array(
                1 => Mage::helper('salesrule')->__('TRUE'),
                0 => Mage::helper('salesrule')->__('FALSE')
            )
        );
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function asHtml()
    {
        $html = $this->getTypeElement()->getHtml();
        $html .= Mage::helper('freento_upsellsrules')->__(
            'If current page is product page for item with %s of these conditions are %s:',
            $this->getAggregatorElement()->getHtml(),
            $this->getValueElement()->getHtml()
        );
        if ($this->getId() != '1') {
            $html.= $this->getRemoveLinkHtml();
        }
        
        return $html;
    }
    
    /**
     * 
     * @return array
     */
    public function getNewChildSelectOptions()
    {
        $productCondition = Mage::getModel('freento_upsellsrules/rule_condition_product');
        $productAttributes = $productCondition->loadAttributeOptions()->getAttributeOption();
        $pAttributes = array();
        foreach ($productAttributes as $code=>$label) {
            if (strpos($code, 'quote_item_') !== 0) {
                $pAttributes[] = array('value'=>'freento_upsellsrules/rule_condition_product|'.$code, 'label'=>$label);
            }
        }

        $conditions = parent::getNewChildSelectOptions();
        $conditions = array(
            array(
                'value' => '',
                'label' => Mage::helper('rule')->__('Please choose a condition to add...')
            ),
            array(
                'value' => 'catalogrule/rule_condition_combine',
                'label' => Mage::helper('catalog')->__('Conditions Combination')
            ),
            array(
                'label' => Mage::helper('catalog')->__('Product Attribute'),
                'value' => $pAttributes
            ),
        );
        return $conditions;
    }
    
    /**
     * 
     * @param Varien_Object $object
     * @return boolean
     */
    public function validate(Varien_Object $object)
    {
        if ($product = $object->getProduct()) {
            return parent::validate($product);
        }
        
        return false;
    }
}