<?php

class Freento_UpsellsRules_Model_Rule_Condition_Category_Page_Combine extends Mage_Rule_Model_Condition_Combine
{
    
    public function __construct()
    {
        parent::__construct();
        $this->setType('freento_upsellsrules/rule_condition_category_page_combine');
    }
    
    /**
     * 
     * @return $this
     */
    public function loadValueOptions()
    {
        $this->setValueOption(
            array(
                1 => Mage::helper('salesrule')->__('TRUE'),
                0 => Mage::helper('salesrule')->__('FALSE')
            )
        );
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function asHtml()
    {
        $html = $this->getTypeElement()->getHtml();
        $html .= Mage::helper('freento_upsellsrules')->__(
            'If current page is category page for item with %s of these conditions are %s:',
            $this->getAggregatorElement()->getHtml(),
            $this->getValueElement()->getHtml()
        );
        
        if ($this->getId() != '1') {
            $html .= $this->getRemoveLinkHtml();
        }
        
        return $html;
    }
    
    /**
     * 
     * @return array
     */
    public function getNewChildSelectOptions()
    {
        $categoryCondition = Mage::getModel('freento_upsellsrules/rule_condition_category');
        $categoryAttributes = $categoryCondition->loadAttributeOptions()->getAttributeOption();
        $cAttributes = array();
        foreach ($categoryAttributes as $code => $label) {
            if (strpos($code, 'quote_item_') !== 0) {
                $cAttributes[] = array(
                    'value' => 'freento_upsellsrules/rule_condition_category|'.$code,
                    'label' => $label
                );
            }
        }

        $conditions = parent::getNewChildSelectOptions();
        $conditions = array(
            array(
                'value' => '',
                'label' => Mage::helper('rule')->__('Please choose a condition to add...')
            ),
            /** @todo add conditions combination */ 
            /*array(
                'value' => 'catalogrule/rule_condition_category_combine',
                'label' => Mage::helper('catalog')->__('Conditions Combination')
            ),*/
            array(
                'value' => $cAttributes,
                'label' => Mage::helper('catalog')->__('Category Attribute')
            ),
        );
        return $conditions;
    }
    
    /**
     * 
     * @param Varien_Object $object
     * @return boolean
     */
    public function validate(Varien_Object $object)
    {
        if ($category = $object->getCategory()) {
            return parent::validate($category);
        }
        
        return false;
    }
}