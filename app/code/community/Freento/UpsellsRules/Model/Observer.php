<?php

class Freento_UpsellsRules_Model_Observer
{
    
    const WIDGET_VALIDATION_FLAG = 'freento_upsells_widget_validation_flag';
    
    /**
     * add discount fields to rule configuration
     * 
     * @param Varien_Event_Observer $observer
     */
    public function addFieldsToGeneralTab($observer)
    {
        $form = $observer->getForm();
        $fieldset = $form->getElement('base_fieldset');

        $fieldset->addField(
            'discount',
            'text',
            array(
                'label' => Mage::helper('freento_upsellsrules')->__('Apply Discount To Upsells'),
                'title' => Mage::helper('freento_upsellsrules')->__('Apply Discount To Upsells'),
                'name' => 'discount'
            )
        );
        
        $fixedLabel = Mage::helper('freento_upsellsrules')->__('Fixed');
        $percentLabel = Mage::helper('freento_upsellsrules')->__('Percent');
        $fieldset->addField(
            'discount_type',
            'select',
            array(
                'label' => Mage::helper('freento_upsellsrules')->__('Discount Type'),
                'title' => Mage::helper('freento_upsellsrules')->__('Discount Type'),
                'values' => array(
                    Freento_UpsellsRules_Helper_Data::DISCOUNT_TYPE_FIXED => $fixedLabel,
                    Freento_UpsellsRules_Helper_Data::DISCOUNT_TYPE_PERCENT => $percentLabel,
                ),
                'name' => 'discount_type'
            )
        );
    }
    
    /**
     * add conditions to rule
     * 
     * @param Varien_Event_Observer $observer
     */
    public function addChildSelectOptions($observer)
    {
        
        $newConditions = array(
            array(
                'value' => 'freento_upsellsrules/rule_condition_product_page_combine',
                'label' => Mage::helper('freento_upsellsrules')->__('Product Page Conditions')
            ),
            array(
                'value' => 'freento_upsellsrules/rule_condition_category_page_combine',
                'label' => Mage::helper('freento_upsellsrules')->__('Category Page Conditions')
            ),
            array(
                'value' => 'freento_upsellsrules/rule_condition_customer_combine',
                'label' => Mage::helper('freento_upsellsrules')->__('Customer Conditions')
            ),
        );
        
        $observer->getAdditional()->setConditions($newConditions);
        
    }
    
    /**
     * 
     * add discount to product
     * 
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function prepareDiscount($observer)
    {
        $item = $observer->getQuoteItem();
        if (is_string($item)) {
            return;
        }
        
        $product = $item->getProduct();
        $customOption = $product->getCustomOption('freento_upsell_rule');
        if ($customOption) {
            $upsellRule = Mage::getSingleton('freento_upsells/rule')->load($customOption->getValue());
            if ($upsellRule && $upsellRule->getId()) {
                $this->calculate($upsellRule, $item);
            }
        }
    }
    
    /**
     * 
     * calculate final price with rule discount
     * 
     * @param Freento_Upsells_Model_Rule $rule
     * @param Mafe_Sales_Model_Quote_Item $item
     */
    public function calculate($rule, $item)
    {
        $finalPrice = $item->getProduct()->getFinalPrice();
        if ($rule->getDiscountType() == Freento_UpsellsRules_Helper_Data::DISCOUNT_TYPE_PERCENT) {
            $specialPrice = $finalPrice - ($finalPrice * $rule->getDiscount() / 100);
        } else {
            $specialPrice = $finalPrice - $rule->getDiscount();
        }
        
        if ($specialPrice > 0) {
            $item->setCustomPrice($specialPrice);
            $item->setOriginalCustomPrice($specialPrice);
            $item->getProduct()->setIsSuperMode(true);
        }
    }
    
    /**
     * add custom scripts to Freento_Upsells admin pages custom scripts block
     * 
     * @param Varien_Event_Observer $observer
     */
    public function addCustomScripts($observer)
    {
        $block = $observer->getBlock();
        
        $scriptsBlock = Mage::app()->getLayout()->createBlock('core/template', 'freento_upsellsrules_scripts');
        $scriptsBlock->setTemplate('freento/upsellsrules/scripts.phtml');
        
        $block->append($scriptsBlock, 'freento_upsellsrules_scripts');
    }

}