<?php

$installer = $this;
$installer->startSetup();

$installer->run(
    "CREATE TABLE IF NOT EXISTS {$this->getTable('freento_upsells/rule')} (
         `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
         `name` varchar(255) DEFAULT NULL,
         `conditions_serialized` text NOT NULL,
         `actions_serialized` text NOT NULL,
         PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$installer->endSetup();