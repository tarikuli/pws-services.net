<?php

class Freento_Upsells_Controller_Widget extends Mage_Core_Controller_Front_Action
{
    
    public function indexAction()
    {
        
        $html = '';
        
        $params = $this->getRequest()->getPost();
        $type = $this->getRequest()->getPost('type', 'freento_upsells/widget_catalog_large');
        $block = Mage::getBlockSingleton($type);
        $html = $block->addData($params)->toHtml();
        
        $this->getResponse()->setBody($html);
    }
    
    /**
     * add all upsell products to shopping cart
     */
    public function addallAction()
    {
        
        if (!$this->_validateFormKey()) {
            $this->_redirectReferer();
            return;
        }
        
        $cart = Mage::getSingleton('checkout/cart');
        
        $productIds = $this->getRequest()->getParam('product_ids');
        $productIds = explode(',', $productIds);
        
        $saveCart = false;
        
        foreach ($productIds as $productId) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productId);
            if ($product->getId() && $product->isSalable()) {
                $params = array(
                    'qty' => 1,
                );
                try {
                    $cart->addProduct($product, $params);
                    $saveCart = true;
                } catch (Exception $e) {
                    Mage::getSingleton('core/session')->addError(
                        Mage::helper('freento_upsells')->__(
                            'Can not add product <strong>%s</strong> to cart',
                            $product->getName()
                        )
                    );
                }
            }
        }
        
        if ($saveCart) {
            $cart->save();
            
            Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
        }
        
        Mage::getSingleton('core/session')->addSuccess(
            Mage::helper('freento_upsells')->__('Product(s) was successfully added to cart')
        );
        
        $this->_redirect('checkout/cart');
        
    }
    
}