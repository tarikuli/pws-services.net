<?php

class Freento_Upsells_Controller_Adminhtml_Action extends Mage_Adminhtml_Controller_Action
{
    
    protected $_generalAction;
    
    /**
     * load layout if not new or AJAX action
     * 
     * @return $this
     */
    public function preDispatch()
    {
        parent::preDispatch();
        
        $generalAction = $this->getRequest()->getActionName();
        $this->_generalAction = $generalAction;
        
        if ($generalAction == 'new' || $this->getRequest()->isAjax()) {
            return $this;
        }
        
        $this->loadLayout();
        
        return $this;
    }
    
    /**
     * add custom js to extension admin pages if not new or AJAX action and render layout
     * 
     * @return $this
     */
    public function postDispatch()
    {
        
        if ($this->_generalAction == 'new' || $this->getRequest()->isAjax()) {
            return $this;
        }
        
        $this->_addCustomScripts();
        
        parent::postDispatch();
        
        $this->renderLayout();
        
        return $this;
    }
    
    /**
     * append custom js to extension admin pages
     */
    protected function _addCustomScripts()
    {
        
        $scriptsBlock = $this->getLayout()->createBlock('freento_upsells/adminhtml_scripts', 'freento_upsells_scripts');
        
        $block = $this->getLayout()->getBlock('before_body_end');
        $block->append($scriptsBlock, 'freento_upsells_scripts');
        
    }
    
    /**
     * 
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('promo/freento_upsells');
    }
    
}