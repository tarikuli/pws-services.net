<?php

require_once(Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'CartController.php');

class Freento_Upsells_CartController extends Mage_Checkout_CartController
{
    
    /**
     * Initialize product instance from request data
     *
     * @return Mage_Catalog_Model_Product || false
     */
    protected function _initProduct()
    {
        $productId = (int) $this->getRequest()->getParam('product');
        if ($productId) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productId);
            
            if ($upsellsData = $this->getRequest()->getParam('freento_upsells')) {
                $upsellsDataDecoded = json_decode(Mage::helper('freento_upsells')->decryptUrlData($upsellsData), 1);
                if ($upsellsDataDecoded) {
                    if (isset($upsellsDataDecoded['rule_id']) && $upsellsDataDecoded['rule_id']) {
                        $product->addCustomOption('freento_upsell_rule', $upsellsDataDecoded['rule_id']);
                    }
                }
            }
            
            if ($product->getId()) {
                return $product;
            }
        }
        
        return false;
    }
    
}