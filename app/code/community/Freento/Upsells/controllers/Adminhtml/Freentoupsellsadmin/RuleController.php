<?php

class Freento_Upsells_Adminhtml_Freentoupsellsadmin_RuleController extends Freento_Upsells_Controller_Adminhtml_Action
{

    public function indexAction()
    {
        $this->_addContent($this->getLayout()->createBlock('freento_upsells/adminhtml_rule', 'freento_upsells_block'));
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $rule = $this->_initRule();

        Mage::register('freento_upsells_registry', $rule, true);
    }
    
    /**
     * prepare rule byID from request
     * 
     * @return Freento_Upsells_Model_Rule
     */
    protected function _initRule()
    {

        $id = (int) $this->getRequest()->getParam('id');
        $rule = Mage::getModel('freento_upsells/rule')->load($id);

        if ($rule->getId()) {
            $rule->getConditions()->setJsFormObject('rule_conditions_fieldset');
            $rule->getActions()->setJsFormObject('rule_actions_fieldset');
        }

        return $rule;
    }
    
    /**
     * 
     * @return Mage_Adminhtml_Controller_Action
     */
    public function saveAction()
    {

        $req = $this->getRequest();

        try {
            $data = $req->getPost();
            /* Convert dates to MYSQL fromat */
            $this->_prepareForSave($data);

            $data['conditions'] = $data['rule']['conditions'];
            $data['actions'] = $data['rule']['actions'];

            $model = Mage::getModel('freento_upsells/rule');
            if ($id = $req->getParam('id')) {
                $model->load($id);
            }
            
            $model->loadPost($data);
            $model->save();

            $process = Mage::getSingleton('index/indexer')->getProcessByCode('freento_extended');
            $process->setStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX);
            $process->save();

            Mage::getSingleton('adminhtml/session')->addSuccess('Rule rule has been successfully saved');
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage())->setElementData($data);
            return $this->_redirect('*/*/edit', array('id' => $req->getParam('id')));
        }

        if ($req->getParam('back') == 'edit') {
            return $this->_redirect(
                '*/*/edit',
                array('id' => $model->getId(), 'tab' => $this->getRequest()->getParam('tab'))
            );
        }

        $this->_redirect('*/*');
    }
    
    /**
     * 
     * @param array $data
     * @return array
     */
    protected function _prepareForSave(&$data)
    {

        foreach ($data as $key => $info) {
            if (preg_match('#^freento_(.+)$#is', $key, $match)) {
                $data[$match[1]] = $info;
            }
        }
        
        return $data;
    }
    
    /**
     * 
     * @param string $prefix
     */
    public function conditionsAction($prefix = 'conditions')
    {
        $id = $this->getRequest()->getParam('id');
        $typeArr = explode('|', str_replace('-', '/', $this->getRequest()->getParam('type')));
        $type = $typeArr[0];

        $model = Mage::getModel($type)
            ->setId($id)
            ->setType($type)
            ->setRule(Mage::getModel('freento_upsells/rule'))
            ->setPrefix($prefix);
        
        if (!empty($typeArr[1])) {
            $model->setAttribute($typeArr[1]);
        }

        if ($model instanceof Mage_Rule_Model_Condition_Abstract) {
            $model->setJsFormObject($this->getRequest()->getParam('form'));
            $html = $model->asHtmlRecursive();
        } else {
            $html = '';
        }
        
        $this->getResponse()->setBody($html);
    }
    
    public function actionsAction()
    {
        return $this->conditionsAction('actions');
    }
    
    /**
     * 
     * @return Mage_Adminhtml_Controller_Action
     */
    public function deleteAction()
    {

        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('freento_upsells/rule');
                $model->setId($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Rule was successfully deleted')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('adminhtml')->__('Unable to find rule to delete.')
        );
        
        $this->_redirect('*/*/');
    } 
    
}
