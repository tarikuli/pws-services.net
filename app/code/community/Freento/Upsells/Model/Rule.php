<?php

class Freento_Upsells_Model_Rule extends Mage_SalesRule_Model_Rule
{

    protected $_eventPrefix = 'freento_upsells_rule';
    
    protected function _construct()
    {
        $this->_init('freento_upsells/rule');
    }
    
    /**
     * 
     * @return Freento_Upsells_Model_Rule_Condition_Combine
     */
    public function getConditionsInstance()
    {
        return Mage::getModel('freento_upsells/rule_condition_combine');
    }
    
    /**
     * 
     * @return Mage_CatalogRule_Model_Rule_Condition_Combine
     */
    public function getActionsInstance()
    {
        return Mage::getModel('catalogrule/rule_condition_combine');
    }
    
    /**
     * 
     * @return array
     */
    public function toOptionArray()
    {
        $array = array();
        $collection = $this->getCollection();
        foreach ($collection as $rule) {
            $array[] = array(
                'value' => $rule->getId(),
                'label' => $rule->getName()
            );
        }
        
        return $array;
    }
    
    

}
