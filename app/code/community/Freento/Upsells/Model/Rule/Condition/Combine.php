<?php

class Freento_Upsells_Model_Rule_Condition_Combine extends Mage_SalesRule_Model_Rule_Condition_Combine
{
    
    /**
     * 
     * @return array
     */
    public function getNewChildSelectOptions()
    {
        
        $addressCondition = Mage::getModel('freento_upsells/rule_condition_address');
        $addressAttributes = $addressCondition->loadAttributeOptions()->getAttributeOption();
        $attributes = array();
        foreach ($addressAttributes as $code=>$label) {
            $attributes[] = array('value'=>'freento_upsells/rule_condition_address|'.$code, 'label'=>$label);
        }

        $conditions = Mage_Rule_Model_Condition_Combine::getNewChildSelectOptions();
        $conditions = array_merge_recursive(
            $conditions,
            array(
                array(
                    'value'=>'freento_upsells/rule_condition_product_found',
                    'label'=>Mage::helper('salesrule')->__('Product attribute combination')
                ),
                array(
                    'value'=>'salesrule/rule_condition_product_subselect',
                    'label'=>Mage::helper('salesrule')->__('Products subselection')
                ),
                array(
                    'value'=>'freento_upsells/rule_condition_combine',
                    'label'=>Mage::helper('salesrule')->__('Conditions combination')
                ),
                array(
                    'label'=>Mage::helper('salesrule')->__('Cart Attribute'),
                    'value' => $attributes
                ),
            )
        );
        
        $additional = new Varien_Object();
        Mage::dispatchEvent('freento_upsells_rule_condition_combine', array('additional' => $additional));
        if ($additionalConditions = $additional->getConditions()) {
            $conditions = array_merge_recursive($conditions, $additionalConditions);
        }

        return $conditions;
    }
    
    
}