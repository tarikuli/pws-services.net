<?php

class Freento_Upsells_Model_Rule_Condition_Product_Found extends Mage_SalesRule_Model_Rule_Condition_Product_Combine
{
    public function __construct()
    {
        parent::__construct();
        $this->setType('freento_upsells/rule_condition_product_found');
    }

    /**
     * Load value options
     *
     * @return Mage_SalesRule_Model_Rule_Condition_Product_Found
     */
    public function loadValueOptions()
    {
        $this->setValueOption(
            array(
                1 => Mage::helper('salesrule')->__('FOUND'),
                0 => Mage::helper('salesrule')->__('NOT FOUND')
            )
        );
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function asHtml()
    {
        $html = $this->getTypeElement()->getHtml();
        $html .= Mage::helper('salesrule')->__(
            "If an item is %s in the cart with %s of these conditions true:",
            $this->getValueElement()->getHtml(),
            $this->getAggregatorElement()->getHtml()
        );
        if ($this->getId() != '1') {
            $html.= $this->getRemoveLinkHtml();
        }
        
        return $html;
    }

    /**
     * validate
     *
     * @param Varien_Object $object Quote
     * @return boolean
     */
    public function validate(Varien_Object $object)
    {
        $all = $this->getAggregator()==='all';
        $true = (bool)$this->getValue();
        $found = false;
        
        if (!$object instanceof Mage_Sales_Model_Quote) {
            $object = $object->getQuote();
        }
        
        foreach ($object->getAllItems() as $item) {
            $found = $all;
            foreach ($this->getConditions() as $cond) {
                $validated = $cond->validate($item);
                if (($all && !$validated) || (!$all && $validated)) {
                    $found = $validated;
                    break;
                }
            }
            
            if (($found && $true) || (!$true && $found)) {
                break;
            }
        }
        
        if ($found && $true) {
            // found an item and we're looking for existing one
            return true;
        } elseif (!$found && !$true) {
            // not found and we're making sure it doesn't exist
            return true;
        }
        
        return false;
    }
}