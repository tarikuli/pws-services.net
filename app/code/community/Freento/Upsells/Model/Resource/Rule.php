<?php

class Freento_Upsells_Model_Resource_Rule extends Mage_Rule_Model_Resource_Abstract
{
    
    protected function _construct()
    {
        $this->_init('freento_upsells/rule', 'id');       
    }
    
}