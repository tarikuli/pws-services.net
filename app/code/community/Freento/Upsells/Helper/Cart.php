<?php

class Freento_Upsells_Helper_Cart extends Mage_Checkout_Helper_Cart
{
    
    /**
     * Retrieve url for add product to cart
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $additional
     * @return string
     */
    public function getAddUrl($product, $additional = array())
    {
        $routeParams = array(
            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->_getHelperInstance('core')
                ->urlEncode($this->getCurrentUrl()),
            'product' => $product->getEntityId(),
            Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey()
        );
        
        if ($ruleId = $product->getFreentoUpsellsRuleId()) {
            $upsellsData = array (
                'rule_id' => $ruleId,
                'random' => rand(),
            );
            $upsellsDataEncrypted = Mage::helper('freento_upsells')->encryptUrlData(json_encode($upsellsData));
            $routeParams['freento_upsells'] = $upsellsDataEncrypted;
        }
        
        if (!empty($additional)) {
            $routeParams = array_merge($routeParams, $additional);
        }

        if ($product->hasUrlDataObject()) {
            $routeParams['_store'] = $product->getUrlDataObject()->getStoreId();
            $routeParams['_store_to_url'] = true;
        }

        if ($this->_getRequest()->getRouteName() == 'checkout'
            && $this->_getRequest()->getControllerName() == 'cart') {
            $routeParams['in_cart'] = 1;
        }

        return $this->_getUrl('freento_upsells/cart/add', $routeParams);
    }
    
}