<?php

class Freento_Upsells_Helper_Data extends Mage_Core_Helper_Abstract
{
    
    /**
     * add current product/category data to widget data
     * 
     * @param Mage_Core_Block_Template $block
     * @return array
     */
    public function getWidgetParams(Mage_Core_Block_Template $block)
    {
        $data = $block->getData();
        
        if ($product = Mage::registry('current_product')) {
            $data['product_id'] = $product->getId();
        }
        
        if (!$product && $category = Mage::registry('current_category')) {
            $data['category_id'] = $category->getId();
            
            $data['category_products'] = implode(',', $category->getProductCollection()->getAllIds());
        }
        
        return $data;
    }
    
    /**
     * 
     * @param string $data
     * @return string
     */
    public function encryptUrlData($data)
    {
        return Mage::helper('core')->urlEncode(Mage::helper('core')->encrypt($data));
    }
    
    /**
     * 
     * @param string $data
     * @return string
     */
    public function decryptUrlData($data)
    {
        return Mage::helper('core')->decrypt(Mage::helper('core')->urlDecode($data));
    }
    
}