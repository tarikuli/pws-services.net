<?php

class Freento_Upsells_Block_Adminhtml_Rule extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        
        parent::__construct();
        
        $this->_controller = 'adminhtml_rule';
        $this->_blockGroup = 'freento_upsells';
        $this->_headerText = $this->__('Upsell Rules');
         
    }

}