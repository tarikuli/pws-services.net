<?php

class Freento_Upsells_Block_Adminhtml_Scripts extends Mage_Core_Block_Template
{
    
    protected function _construct()
    {
        parent::_construct();
        
        $this->setTemplate('freento/upsells/scripts.phtml');
        
        return $this;
    }
    
    /**
     * method gives submodules possibility to add child blocks to $this
     * 
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        Mage::dispatchEvent('freento_upsells_addcustomscripts', array('block' => $this));
        
        return parent::_beforeToHtml();
    }
    
}