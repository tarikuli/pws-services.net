<?php

class Freento_Upsells_Block_Adminhtml_Rule_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    
    /**
     * 
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('base_fieldset', array('legend' => $this->__('Rule settings')));
        $form->setHtmlIdPrefix('freento_upsells_');

        $rule = Mage::registry('freento_upsells_registry');

        $fieldset->addField(
            'name',
            'text',
            array(
                'name' => 'name',
                'label' => $this->__('Rule Name'),
                'title' => $this->__('Rule Name'),
            )
        );
        
        Mage::dispatchEvent('freento_upsells_rule_edit_tab_general_prepare_form', array('form' => $form));
        
        if (Mage::getSingleton('adminhtml/session')->getElementData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getElementData());
            Mage::getSingleton('adminhtml/session')->setElementData(null);
        } else {
            $form->setValues($rule->getData());
        }

        $this->setForm($form);
        return parent::_prepareForm();
    }

}
