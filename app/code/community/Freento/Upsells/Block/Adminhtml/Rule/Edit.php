<?php

class Freento_Upsells_Block_Adminhtml_Rule_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {

        parent::__construct();
        /* Add path to form */
        $this->_blockGroup = 'freento_upsells';
        $this->_controller = 'adminhtml_rule';
        $this->_mode = 'edit';
        
        $this->_addButton(
            'save_and_continue_edit',
            array(
                'class'   => 'save',
                'label'   => Mage::helper('salesrule')->__('Save and Continue Edit'),
                'onclick' => 'editForm.submit($(\'edit_form\').action + \'back/edit/\')',
            ),
            10
        );
        
    }
    
    /**
     * 
     * @return string
     */
    public function getHeaderText()
    {
        $rule = Mage::registry('freento_upsells_registry');
        if ($rule->getId()) {
            return Mage::helper('salesrule')->__("Edit Rule '%s'", $this->escapeHtml($rule->getName()));
        } else {
            return Mage::helper('salesrule')->__('New Rule');
        }
    }

}
