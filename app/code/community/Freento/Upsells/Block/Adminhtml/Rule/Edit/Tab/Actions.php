<?php

class Freento_Upsells_Block_Adminhtml_Rule_Edit_Tab_Actions extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * 
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('rule_');

        $rule = Mage::registry('freento_upsells_registry');

        $form->addFieldset('actions_fieldset', array('legend' => $this->__('Rule Products')))
            ->setRenderer($this->_getRulesFieldset())
            ->addField(
                'actions',
                'text',
                array(
                    'name' => 'actions',
                    'label' => $this->__('Actions'),
                    'title' => $this->__('Actions'),
                    'required' => false,
                )
            )
            ->setRule($rule)
            ->setRenderer($this->getLayout()->getBlockSingleton('rule/actions'));

        if (Mage::getSingleton('adminhtml/session')->getElementData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getElementData());
            Mage::getSingleton('adminhtml/session')->setElementData(null);
        } else {
            $form->setValues($rule->getData());
        }

        $this->setForm($form);
        return parent::_prepareForm();
    }
    
    /**
     * 
     * @return string
     */
    protected function _getChildUrl()
    {
        return $this->getUrl('*/freentoupsellsadmin_rule/actions/form/rule_actions_fieldset');
    }
    
    /**
     * 
     * @return Mage_Core_Block_Abstract
     */
    protected function _getRulesFieldset()
    {
        return $this->getLayout()
            ->getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
            ->setNewChildUrl($this->_getChildUrl())
            ->setTemplate('promo/fieldset.phtml');
    }

}
