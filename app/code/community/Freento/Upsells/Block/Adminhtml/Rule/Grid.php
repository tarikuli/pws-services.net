<?php

class Freento_Upsells_Block_Adminhtml_Rule_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        
        $this->setId('freentoUpsellsGrid');
        $this->setUseAjax(false);
        $this->setSaveParametersInSession(true);
    }
    
    /**
     * 
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('freento_upsells/rule')->getCollection();
         
        $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }
    
    /**
     * 
     * @return $this
     */
    protected function _prepareColumns()
    {

        $this->addColumn(
            'id',
            array(
                'header' => $this->__('ID'),
                'align' => 'left',
                'width' => '50px',
                'index' => 'id'
            )
        );
        
        $this->addColumn(
            'name',
            array(
                'header' => $this->__('Rule Name'),
                'index' => 'name'
            )
        );
        
        return $this;
    }
    
    /**
     * 
     * @param Mage_Catalog_Model_Product|Varien_Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl(
            '*/*/edit',
            array(
                'id' => $row->getId()
            )
        );
    }
    
}