<?php

class Freento_Upsells_Block_Adminhtml_Rule_Edit_Tab_Conditions extends Mage_Adminhtml_Block_Widget_Form
{
    
    /**
     * 
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('rule_');

        $rule = Mage::registry('freento_upsells_registry');
        $legend = $this->__('Apply the rule only if the following conditions are met (leave blank for all products)');
        $form->addFieldset('conditions_fieldset', array('legend' => $legend))
            ->setRenderer($this->_getRulesFieldset())
            ->addField(
                'conditions',
                'text',
                array(
                    'name' => 'conditions',
                    'label' => $this->__('Conditions'),
                    'title' => $this->__('Conditions'),
                    'required' => false,
                )
            )
            ->setRule($rule)
            ->setRenderer($this->getLayout()->getBlockSingleton('rule/conditions'));

        if (Mage::getSingleton('adminhtml/session')->getElementData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getElementData());
            Mage::getSingleton('adminhtml/session')->setElementData(null);
        } else {
            $form->setValues($rule->getData());
        }

        $this->setForm($form);
        return parent::_prepareForm();
    }
    
    /**
     * 
     * @return string
     */
    protected function _getChildUrl()
    {
        return $this->getUrl('*/freentoupsellsadmin_rule/conditions/form/rule_conditions_fieldset');
    }
    
    /**
     * 
     * @return Mage_Core_Block_Abstract
     */
    protected function _getRulesFieldset()
    {
        return $this->getLayout()
                ->getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
                ->setNewChildUrl($this->_getChildUrl())
                ->setTemplate('promo/fieldset.phtml');
    }

}
