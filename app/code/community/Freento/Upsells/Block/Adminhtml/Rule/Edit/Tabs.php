<?php

class Freento_Upsells_Block_Adminhtml_Rule_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('freento_upsells_edit_tabs');
        $this->setDestElementId('edit_form');
    }
    
    /**
     * 
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $block = $this->getLayout()->createBlock('freento_upsells/adminhtml_rule_edit_tab_general')->toHtml();
        $this->addTab(
            'general_section',
            array(
                'label' => $this->__('Rule Dashboard'),
                'title' => $this->__('Rule Dashboard'),
                'content' => $block,
                'active' => true
            )
        );
        
        $block = $this->getLayout()->createBlock('freento_upsells/adminhtml_rule_edit_tab_conditions')->toHtml();
        $this->addTab(
            'conditions_section',
            array(
                'label' => $this->__('Rule Conditions'),
                'title' => $this->__('Rule Conditions'),
                'content' => $block,
            )
        );
        
        $block = $this->getLayout()->createBlock('freento_upsells/adminhtml_rule_edit_tab_actions')->toHtml();
        $this->addTab(
            'actions_section',
            array(
                'label' => $this->__('Show Rule Products'),
                'title' => $this->__('Show Rule Products'),
                'content' => $block,
            )
        );
        
        $this->setActiveTab(preg_replace("/id_/i", "", $this->getRequest()->getParam('tab')));

        return parent::_beforeToHtml();
    }

}
