<?php

class Freento_Upsells_Block_Upsells extends Mage_Catalog_Block_Product_List_Upsell
{
    protected $_rulesCollection = null;

    /**
     * check widget rules actions and return conditions products
     *
     * @return $this
     */
    protected function _prepareData()
    {
        $rules = $this->getData('rules');

        $this->_rulesCollection = Mage::getModel('freento_upsells/rule')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('id', array('in' => explode(',', $rules)));

        $session = Mage::getSingleton('checkout/session');

        $validateObject = new Varien_Object($session->getData());

        $validateObject->setQuote($session->getQuote());
        $validateObject->setBlock($this);

        Mage::dispatchEvent('freento_upsells_widget_validate_before', array('validate_object' => $validateObject));

        $this->_itemCollection = new Varien_Data_Collection();

        $productsCount = 0;

        $productsCollection = Mage::getModel('catalog/product')
            ->getCollection()
            ->setStore(Mage::app()->getStore())
            ->addAttributeToSelect('*');

        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($productsCollection);
        $productsCollection->addAttributeToFilter(
            'visibility',
            array ('in' => array (
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH
            ))
        )
        ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);

        // exclude products
        $excludeProducts = array ();

        if ($productId = $validateObject->getBlock()->getProductId()) {
            $excludeProducts[] = $productId;
        }

        if ($categoryProducts = $validateObject->getBlock()->getCategoryProducts()) {
            $categoryProducts = explode(',', $categoryProducts);
            $excludeProducts = $excludeProducts + $categoryProducts;
        }

        foreach ($session->getQuote()->getAllVisibleItems() as $item) {
            $excludeProducts[] = $item->getProduct()->getId();
        }

        if (!empty($excludeProducts)) {
            $productsCollection->addFieldToFilter('entity_id', array ('nin' => $excludeProducts));
        }

        $ruleIds = array();

        foreach ($this->_rulesCollection as $rule) {
            if ($rule->validate($validateObject)) {
                $ruleIds[$rule->getId()] = $rule;
            }
        }

        if(count($ruleIds))
        {
            $indexerData = Mage::getSingleton('freento_extended/data');
            $productIds = $indexerData->fetchProductIds(array_keys($ruleIds));
            if(count($productIds))
            {
                $productsCollection->addFieldToFilter('entity_id', array ('in' => $productIds));
                foreach ($productsCollection as $product)
                {
                    $ruleId = $indexerData->getRuleId($product->getId());

                    if(isset($ruleIds[$ruleId]))
                    {
                        $rule = $ruleIds[$ruleId];

                        $finalPrice = $product->getPrice();
                        $product->setFreentoUpsellsRuleId($rule->getId());
                        $discountAmount = 0;
                        if ($rule->getDiscountType() == Freento_UpsellsRules_Helper_Data::DISCOUNT_TYPE_PERCENT) {
                            $discountAmount = ($finalPrice * $rule->getDiscount() / 100);
                        } else {
                            $discountAmount = $rule->getDiscount();
                        }

                        #$product->setSpecialPrice($finalPrice - $discountAmount);
                    }

                    $this->_itemCollection->addItem($product);
                    $productsCount++;
                    if ($productsCount >= $this->getProductLimit())
                        break;
                }
            }
        }

        // foreach ($this->_rulesCollection as $rule) {
        //     if ($rule->validate($validateObject)) {
        //         foreach ($productsCollection as $product) {
        //             if ($rule->getActions()->validate($product)) {
        //                 $product->setFreentoUpsellsRuleId($rule->getId());
        //                 $this->_itemCollection->addItem($product);
        //                 $productsCount++;
        //                 if ($productsCount >= $this->getProductLimit()) {
        //                     break(2);
        //                 }
        //             }
        //         }
        //     }
        // }

        Mage::dispatchEvent('freento_upsells_widget_validate_after', array('validate_object' => $validateObject));

        return $this;
    }

    /**
     * Retrieve url for add product to cart
     * Will return product view page URL if product has required options
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $additional
     * @return string
     */
    public function getAddToCartUrl($product, $additional = array())
    {
        if (!$product->getTypeInstance(true)->hasRequiredOptions($product)) {
            return $this->helper('freento_upsells/cart')->getAddUrl($product, $additional);
        }
        $additional = array_merge(
            $additional,
            array(Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey())
        );
        if (!isset($additional['_escape'])) {
            $additional['_escape'] = true;
        }
        if (!isset($additional['_query'])) {
            $additional['_query'] = array();
        }
        $additional['_query']['options'] = 'cart';
        return $this->getProductUrl($product, $additional);
    }
}