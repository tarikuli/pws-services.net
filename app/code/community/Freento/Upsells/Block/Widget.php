<?php

class Freento_Upsells_Block_Widget extends Freento_Upsells_Block_Upsells implements Mage_Widget_Block_Interface
{
    
    /**
     * 
     * @return $this
     */
    protected function _construct()
    {
        parent::_construct();
        
        $this->setTemplate($this->_widgetTemplate);
    }
    
    /**
     * 
     * @return int|string
     */
    public function getColumnCount()
    {
        return $this->getData('column_count');
    }
    
    /**
     * 
     * @return int|string
     */
    public function getProductLimit()
    {
        return $this->getData('product_limit');
    }
    
    /**
     * 
     * @return string
     */
    public function getAjaxRequestUrl()
    {
        return Mage::getUrl(
            'freento_upsells/widget',
            array(
                '_secure' => Mage::app()->getStore()->isCurrentlySecure(),
            )
        );
    }
    
}
