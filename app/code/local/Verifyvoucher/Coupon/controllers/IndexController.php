<?php
class Verifyvoucher_Coupon_IndexController extends Mage_Core_Controller_Front_Action
{

    public function IndexAction()
    {
        $this->loadLayout();
        $this->getLayout()
            ->getBlock("head")
            ->setTitle($this->__("Verify Voucher"));
        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home Page"),
            "title" => $this->__("Home Page"),
            "link" => Mage::getBaseUrl()
        ));
        
        $breadcrumbs->addCrumb("verify voucher", array(
            "label" => $this->__("Verify voucher"),
            "title" => $this->__("Verify voucher")
        ));
        
        $this->renderLayout();
    }
/*
 * https://www.mymonogramonline.com/coupon/index/updatemenu
 */
    public function updatemenuAction()
    {
        $this->loadLayout();
        $this->getLayout()
        ->getBlock("head")
        ->setTitle($this->__("Menu Update Completed"));
        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home Page"),
            "title" => $this->__("Home Page"),
            "link" => Mage::getBaseUrl()
        ));
        
        $breadcrumbs->addCrumb("verify voucher", array(
            "label" => $this->__("Verify voucher"),
            "title" => $this->__("Verify voucher")
        ));
        
        $this->renderLayout();
    }

    
    
    public function saveAction()
    {
        // Call 
        $apiCoupon = Mage::helper('coupon');
        
        // Get coupon_code from Post or Get method
        $coupon_code = (string) trim($this->getRequest()->getParam('coupon_code'));
        
        // Put coupon_code in session
        Mage::getSingleton('core/session')->setData('session_coupon_code', $coupon_code);
        
        // Load rule_id from salesrule_coupon table by code.
        $oCoupon = Mage::getModel('salesrule/coupon')->load($coupon_code, 'code');

        // Convert $oCoupon object to Array
        $coupon_data = $oCoupon->getData();
        
        // Load from salesrule table by rule_id.
        $oRule = Mage::getModel('salesrule/rule')->load($oCoupon->getRuleId());
        
        // Covert conditions_serialized to Array
        $rule_arr = unserialize($oRule->getConditionsSerialized());
        $conditions_arr = isset($rule_arr['conditions'])? $rule_arr['conditions']: []; // $rule_arr['conditions'];
        
        
        if ((!$oCoupon->getRuleId()) || (!$oRule->getIsActive()) ) {
            Mage::log($coupon_code . " = can't seem to find this code in our database from verify page. ".(isset($_SERVER['HTTP_X_FORWARDED_FOR'])? $_SERVER['HTTP_X_FORWARDED_FOR']: ''), null, 'jewel_coupon.log');
            
            $html = "<p><strong>Sorry but we can't seem to find this code in our database. </strong></p>
                     <p>Please make sure that the code has been entered correctly. </p>
                     <br>
                     <p>Please note : All promotional coupon /voucher codes start with es, gn, gg, g5, g4, or g2 etc.</p>
                     <p>The information starting with '1000-' or 'LG-' is receipt # for your purchase of the code and</p>
                     <p>cannot be applied as coupon.</p>
                     <p>Please refer to your Social Deal Site for the correct coupon /voucher code</p>
";
            
            Mage::getSingleton('core/session')->setData('EM', $html);
            
            $this->_redirect('coupon');

        } elseif ($coupon_data['times_used'] == 1) {
            
            $orderCollection = Mage::getModel('sales/order')->getCollection()
                                                            ->addAttributeToSelect('increment_id')
                                                            ->addAttributeToSelect('customer_firstname')
                                                            ->addAttributeToSelect('customer_lastname')
                                                            ->addAttributeToSelect('customer_email')
                                                            ->addAttributeToSelect('created_at')
                                                            ->addAttributeToFilter('coupon_code', $coupon_code);
            $orderInfo = $orderCollection->getData();
            
            $couponMessage = $apiCoupon->getStoreLabels($oCoupon->getRuleId());
            
            # echo "<pre>"; print_r($rule_arr); print_r( $orderCollection->getData()); echo "</pre>"; die();
            
            if(isset($orderInfo[0]['created_at'])){
                $html = '<p><strong>We found your code in the our database but it was already redeemed on : '. Mage::getModel('core/date')->date('M d, Y', $orderInfo[0]['created_at']).', Order# '.$orderInfo[0]['increment_id'].'</strong></p>
                     ';
            }else {
                $html = '<p><strong>We found your code in the our database but it was already redeemed.<strong></p>
                     <p>Your coupon was valid for : '. $couponMessage .' From the following collection</p>
                     ';
            }

            
            Mage::getSingleton('core/session')->setData('EM', $html);
            
            $this->_redirect('coupon');
            
        } else {
            
            if (count($coupon_data) > 0) {

                
                $couponMessage = $apiCoupon->getStoreLabels($oCoupon->getRuleId());
                
                
                $product_arr = array();
                
                foreach ($conditions_arr as $con_key => $con_value) {
                    if(isset($con_value['conditions'])){
                        foreach ($con_value['conditions'] as $condition_key => $condition_value) {
                            
                            if ($condition_value['attribute'] == "sku" && isset($con_value['conditions']) && isset($condition_value['value'])) {
                                $sku_arr = explode(',', $condition_value['value']);
                                
                                $products = Mage::getResourceModel('catalog/product_collection')->addAttributeToSelect(array(
                                    'name',
                                    'url_path',
                                    'thumbnail',
                                    'price'
                                ))
                                ->addAttributeToFilter('sku', array(
                                    'in' => $sku_arr
                                ))
                                ->load();
                                
                                // usage
                                $product_data = [];
                                
                                foreach ($products as $prod) {
                                    $product_data['sku'] = $prod->getSku();
                                    $product_data['name'] = $prod->getName();
                                    $product_data['url_path'] = $prod->getUrlPath();
                                    $product_data['price'] = $prod->getPrice();
                                    $product_data['image_path'] = (string) Mage::helper('catalog/image')->init($prod, 'thumbnail');
                                    array_push($product_arr, $product_data);
                                }
                            }else {
                                Mage::log($coupon_code . " = has incorrect rule from verify page. ".(isset($_SERVER['HTTP_X_FORWARDED_FOR'])? $_SERVER['HTTP_X_FORWARDED_FOR']: ''), null, 'jewel_coupon.log');
                            }
                        }
                    }else{
                        Mage::log($coupon_code . " = No condition found. ".(isset($_SERVER['HTTP_X_FORWARDED_FOR'])? $_SERVER['HTTP_X_FORWARDED_FOR']: ''), null, 'jewel_coupon.log');
                    }
                }
                
                Mage::getSingleton('core/session')->setData('session_product_data', $product_arr);
                
                $html = "<p><strong>Yay! We found your voucher in our system</strong><p>
                         <p>Your coupon is valid for : ".$couponMessage." From the following collection<p>
                         <br>
                         <p>Important Note:</p>
                         <p>When Processing a voucher code for MULTIPLE ITEMS it is necessary to click on the 'Keep shopping' button<p>
                         <p>in the shopping cart to enter the additional designs/personalization<p>
                        ";
                
                Mage::getSingleton('core/session')->setData('EM', $html);
                
                $this->_redirect('coupon');
                
            } else {
                $html = '<p>Please accept our apologies but the promotional codes (redemption certificates) start with ck, es, gn, gg, g5, g4, or g2 etc.<p>
                         <p>The information provided that starts with 1000- OR LG- is the receipt for your purchase of the discount code which will not work in our system.<p>';
                
                Mage::getSingleton('core/session')->setData('EM', $html);
                $this->_redirect('coupon');
            }
        }
    }
}