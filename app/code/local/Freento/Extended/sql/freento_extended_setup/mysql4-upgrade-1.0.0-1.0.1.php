<?php

$installer = $this;
$installer->startSetup();

$installer->run(
    "CREATE TABLE IF NOT EXISTS {$this->getTable('freento_extended/data')} (
         `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
         `rule_id` int(11) unsigned DEFAULT NULL,
         `product_id` int(11) unsigned NOT NULL,
         PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$installer->endSetup();
