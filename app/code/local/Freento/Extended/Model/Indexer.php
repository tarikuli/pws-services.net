<?php

ini_set('memory_limit','1024M');

class Freento_Extended_Model_Indexer extends Mage_Index_Model_Indexer_Abstract
{
    protected $_matchedEntities = array(
        'freento_extended_entity' => array(
            Mage_Index_Model_Event::TYPE_SAVE
        )
    );

    // var to protect multiple runs
    protected $_registered = false;
    protected $_processed = false;

    public function matchEvent(Mage_Index_Model_Event $event)
    {
        return Mage::getModel('catalog/category_indexer_product')->matchEvent($event);
    }

    public function getName()
    {
        return Mage::helper('freento_extended')->__('Freento Extended');
    }

    public function getDescription()
    {
        return Mage::helper('freento_extended')->__('Collect all product IDs to fatch at checkout cart page.');
    }

    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
    }

    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        // process index event
        if(!$this->_processed)
        {
            $this->_processed = true;
        }
    }

    public function reindexAll()
    {
        $data = array();

        $rules = Mage::getModel('freento_upsells/rule')->getCollection()
            ->addFieldToSelect('*');

        $productsCollection = Mage::getModel('catalog/product')
            ->getCollection()
            ->setStore(Mage::app()->getStore())
            ->addAttributeToSelect('*');

        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($productsCollection);
        $productsCollection->addAttributeToFilter(
            'visibility',
            array ('in' => array (
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH
            ))
        )
        ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);

        foreach ($rules as $rule)
        {
            foreach ($productsCollection as $product)
            {
                if ($rule->getActions()->validate($product))
                    $data[$rule->getId()][] = $product->getId();
            }
        }

        $indexerData = Mage::getSingleton('freento_extended/data');
        $indexerData->insertData($data);
    }
}