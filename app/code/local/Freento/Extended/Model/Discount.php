<?php

class Freento_Extended_Model_Discount extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
	public function collect(Mage_Sales_Model_Quote_Address $address)
	{
		if ($address->getData('address_type') == 'billing')
			return $this;

		$cart = $address->getQuote();

		$discount = Mage::helper('freento_extended')->getDiscountFromQuote($cart);

		$totals = array_sum($address->getAllTotalAmounts());
        $baseTotals = array_sum($address->getAllBaseTotalAmounts());

		$address->setGrandTotal($totals + $discount);
		$address->setBaseGrandTotal($baseTotals + $discount);
		return $this;
	}

	public function fetch(Mage_Sales_Model_Quote_Address $address)
	{
		if ($address->getData('address_type') == 'billing')
			return $this;

		$cart = $address->getQuote();
		$discount = Mage::helper('freento_extended')->getDiscountFromQuote($cart);

		if ($discount != 0) {
			$address->addTotal(array(
	            'code' => Freento_Extended_Helper_Data::CODE,
	            'title' => Freento_Extended_Helper_Data::LABEL,
	            'value' => $discount
	        ));
	    }

	    return $address;
	}
}