<?php

class Freento_Extended_Model_Resource_Data extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('freento_extended/data', 'id');
    }

    public function insertAllData($data)
    {
    	$adapter = $this->_getWriteAdapter();

    	foreach($data as $ruleId => $productIds)
    	{
            $adapter->delete($this->getTable('freento_extended/data'), array(
                'rule_id=?' => $ruleId
            ));

            $products = array();
            foreach ($productIds as $productId) {
                $products[] = array(
                    'rule_id' => (int)$ruleId,
                    'product_id'  => (int)$productId,
                );
            }

            $adapter->insertMultiple($this->getTable('freento_extended/data'), $products);
    	}

    	return $this;
    }

    public function fetchProductIds($ruleIds)
    {
        $read = $this->_getReadAdapter();

        $ruleIds = implode(",", $ruleIds);

        $select = $read->select()
            ->from($this->getTable('freento_extended/data'), array('product_id'))
            ->where('rule_id IN (' . $ruleIds . ')');

        return $read->fetchCol($select, array());
    }

    public function getRuleId($productId)
    {
        $read = $this->_getReadAdapter();

        $select = $read->select()
            ->from($this->getTable('freento_extended/data'), array('rule_id'))
            ->where('product_id = ' . $productId);

        return $read->fetchOne($select, array());
    }
}
