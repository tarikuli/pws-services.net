<?php

class Freento_Extended_Model_Invoice extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
    	$order = $invoice->getOrder();

    	$discount = Mage::helper('freento_extended')->getDiscountFromOrder($order);
        $invoice->setGrandTotal($invoice->getGrandTotal() + $discount);
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $discount);

        return $this;
    }
}