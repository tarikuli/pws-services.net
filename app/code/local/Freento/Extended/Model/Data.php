<?php

class Freento_Extended_Model_Data extends Mage_Core_Model_Abstract
{
    protected $_eventPrefix = 'freento_upsells_rule';

    protected function _construct()
    {
        $this->_init('freento_extended/data');
    }

    public function insertData($data)
    {
        return $this->_getResource()->insertAllData($data);
    }

    public function fetchProductIds($ruleIds)
    {
        return $this->_getResource()->fetchProductIds($ruleIds);
    }

    public function getRuleId($productId)
    {
        return $this->_getResource()->getRuleId($productId);
    }
}
