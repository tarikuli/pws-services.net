<?php

class Freento_Extended_Model_Creditmemo extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
    	$order = $creditmemo->getOrder();

    	$discount = Mage::helper('freento_extended')->getDiscountFromOrder($order);
        $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $discount);
        $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $discount);
        return $this;
    }
}