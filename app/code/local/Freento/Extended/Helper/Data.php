<?php

class Freento_Extended_Helper_Data extends Mage_Core_Helper_Abstract
{
	const CODE = 'freento_discount';
	const LABEL = 'Discount (Upsell)';

	public function getDiscountFromQuote($cart)
	{
		$discount = 0;
		foreach ($cart->getAllItems() as $item)
		{
		    $amount = $item->getCustomDiscountAmount();

		    if(isset($amount) && trim($amount) != '')
		    	$discount += floatval($amount);
		}

		$discount *= -1;
		return $discount;
	}

	public function getDiscountFromOrder($order)
	{
		$discount = 0;
		foreach ($order->getAllItems() as $item)
		{
		    $amount = $item->getCustomDiscountAmount();

		    if(isset($amount) && trim($amount) != '')
		    	$discount += floatval($amount);
		}

		$discount *= -1;
		return $discount;
	}
}