<?php

class Freento_Extended_Block_Sales_Creditmemo extends Mage_Sales_Block_Order_CreditMemo_Totals
{
    protected function _initTotals()
    {
        parent::_initTotals();

        $order = $this->getSource()->getOrder();

        $discount = Mage::helper('freento_extended')->getDiscountFromOrder($order);
        if ($discount != 0) {
            $this->addTotal(new Varien_Object(array(
                'code' => Freento_Extended_Helper_Data::CODE,
                'value' => $discount,
                'base_value' => $discount,
                'label' => Freento_Extended_Helper_Data::LABEL,
            )), Freento_Extended_Helper_Data::CODE);
        }
        return $this;
    }
}