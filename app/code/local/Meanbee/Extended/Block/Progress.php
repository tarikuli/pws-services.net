<?php

class Meanbee_Extended_Block_Progress extends Mage_Core_Block_Template
{
	protected $_minPackageValue = 0;

	protected function getFreeShippingRules()
	{
		return Mage::getModel('meanship/rule')->getCollection()
			->addFieldToFilter('price', 0)
			->addFieldToFilter('is_active', 1);
	}

	public function checkFreeShipping()
	{
		$rules = $this->getFreeShippingRules();

		$packageValues = array(); $promoRuleIds = array();
		foreach($rules as $rule)
		{
			$conditions = $rule->getConditionsSerialized();

			if(isset($conditions) && $conditions != "")
			{
				$conditions = unserialize($conditions);

				if(isset($conditions['conditions']))
				{
					foreach($conditions['conditions'] as $condition)
					{
						if($condition['attribute'] == 'package_value_with_discount')
							$packageValues[] = floatval($condition['value']);
						else if($condition['attribute'] == 'promo_applied_rule_ids')
						{
							if(is_array($condition['value']))
								$promoRuleIds = array_merge($promoRuleIds, $condition['value']);
							else
								$promoRuleIds[] = $condition['value'];
						}
					}
				}
			}
		}

		$promoRuleIds = array_unique($promoRuleIds);

		if(count($promoRuleIds))
		{
			$couponCode = Mage::getModel('checkout/cart')->getQuote()->getCouponCode();

			if(isset($couponCode))
			{
				$oCoupon = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');
				$ruleId = $oCoupon->getRuleId();

				if(in_array($ruleId, $promoRuleIds))
					return true;
			}
		}

		if(count($packageValues))
		{
			$this->_minPackageValue = min($packageValues);
			$subtotal = Mage::getModel('checkout/cart')->getQuote()->getGrandTotal();
			if($subtotal >= $this->_minPackageValue)
				return true;
		}

		return false;
	}

	public function getSubtotal()
	{
	    return Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal();
	}

	public function getMinPackageValue()
	{
		if($this->_minPackageValue == 0)
			$this->checkFreeShipping();

		return $this->_minPackageValue;
	}

	public function formatPrice($price)
	{
		return Mage::helper('core')->currency($price, true, false);
	}
}
