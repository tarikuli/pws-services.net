<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Feed
 */  
class Amasty_Feed_Model_Writer_Xml extends Amasty_Feed_Model_Writer_Abstract
{
    protected $custom_label_1;
    
    function write(){
        
        if ($this->isFirstStep()){
            $this->header();
        }
        
        $ret = parent::write();
        
        if ($this->isLastStep()){
            $this->footer();
        }
        
        return $ret;
    }
    
    public function writeRecord($row)
    {
        $writes = array();
        $item = $this->_getFeed()->getXmlItem();
        $fields = $this->_getFeed()->getFields();
        foreach($this->_getFeed()->getLines2fields() as $lines2field){
            $write = $lines2field['tpl'];
            $skip = TRUE;

            if(isset($lines2field['vars'])){
                foreach($lines2field['vars'] as $varOrder => $var){
                    
                    $link = $lines2field['links'][$varOrder];
                    $optional = isset($fields['optional'][$link]) ? ($fields['optional'][$link] == 'yes') : FALSE;
                    $value = $row[$link];
                    
                    $customVariable = explode("{",$write);
                    if(isset($customVariable[0]) && ($customVariable[0] == "<g:custom_label_2>")){
//                         Mage::log("links before --- ".$customVariable[0]." ---- ".$value, null, 'system.log');
                        $value = str_replace(","," &gt; ",$value);
                        
//                         Mage::log("links after --- ".$customVariable[0]." ---- ".$value, null, 'system.log');
                    }
                    
                    $skip = $skip && $optional && $value == '';

                    if (!$skip){
//                         Mage::log('writeRecord '.date('Y-m-d H:i:s')."  ---- ".$write." ------".$value." ---".$var, null, 'system.log');
                        
                        if(isset($customVariable[0]) && ($customVariable[0] == "<g:custom_label_2>")){
                            $this->custom_label_1 = str_replace('{' . $var . '}', $value, $write); 
//                             Mage::log("links g:custom_label_2 --- ".$customVariable[0]." ---- ".$value."---".$this->custom_label_1, null, 'system.log');
                        }
                        
                        if(isset($customVariable[0]) && ($customVariable[0] == "<g:product_type>")){
//                             Mage::log($value." ---- ".$var, null, 'system.log');
                            $write = str_replace('g:custom_label_2', 'g:product_type', $this->custom_label_1); 
//                             Mage::log($write, null, 'system.log');
                        }else{
                            $write = str_replace('{' . $var . '}', $value, $write); 
                        }

                    }else{
                        $write = '';
                    }
                }
                
                $writes[] = $write;
            }

        }

        $out = !empty($item) ?
                "<" . $item . ">" . implode('', $writes) . "</" . $item . ">" :
                implode('', $writes);    

        fwrite($this->fp, $out);
    }
    
    public function header(){
        fwrite($this->fp, $this->_getFeed()->getXmlHeader());
    }
    
    public function footer(){
        fwrite($this->fp, $this->_getFeed()->getXmlFooter());
    }
}