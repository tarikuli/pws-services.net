<?php 

class Tm_SpecialPriceCountdown_Helper_Data extends Mage_Core_Helper_Data
{
	const XML_PATH_PRODUCT_VIEW_ENABLED  = 'specialpricecountdown/general/active';
	const XML_PATH_GRIDLIST_ENABLED 	 = 'specialpricecountdown/gridlist/active';
	const XML_PATH_SALES_ENABLED 		 = 'specialpricecountdown/sales/active';
	const XML_PATH_PRODUCT_VIEW_SELECTOR = 'specialpricecountdown/general/selector';
	
	private function config($code, $store = null){
		return Mage::getStoreConfig($code, $store);
	}

	private function getStore()
	{
		return Mage::app()->getStore();
	}

	public function getSelector($store = null)
	{
		return $this->config(self::XML_PATH_PRODUCT_VIEW_SELECTOR, $store);
	}

	public function isEnabledProductView($store = null){
		return $this->config(self::XML_PATH_PRODUCT_VIEW_ENABLED, $store);
	}

	public function isEnabledGridList($store = null){
		return $this->config(self::XML_PATH_GRIDLIST_ENABLED, $store);
	}
	public function isEnabledSales($store = null){
		return $this->config(self::XML_PATH_SALES_ENABLED, $store);
	}

	public function getProduct()
	{
		$productModel = Mage::getModel('catalog/product');
		$product = $productModel->load(Mage::registry('current_product')->getId());
		return $product; 
	}

	public function countdownTime($product)
 	{
		$special_counter_time = $product->getSpecialToDate();    
		return $special_counter_time;
	}

	public function currDate()
	{
		return $currDate = date("Y-m-d H:i:s");
	}
}
?>