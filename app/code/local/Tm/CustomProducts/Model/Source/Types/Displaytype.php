<?php

class Tm_CustomProducts_Model_Source_Types_Displaytype
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'viewed_products', 'label'=>Mage::helper('adminhtml')->__('Most Viewed Products')),
            array('value' => 'best_products', 'label'=>Mage::helper('adminhtml')->__('Bestsellers Products')),
			array('value' => 'sale_products', 'label'=>Mage::helper('adminhtml')->__('Sale Product'))
        );
    }
}