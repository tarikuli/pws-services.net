<?php

class Tm_CustomProducts_Block_Info
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{
	
	const DISPLAY_TYPE_VIEWED_PRODUCTS         = 'viewed_products';
    const DISPLAY_TYPE_BEST_PRODUCTS         = 'best_products';
	const DISPLAY_TYPE_SALE_PRODUCTS         = 'sale_products';
	
   protected function _construct()
    {
       parent::_construct();
    }

    protected function _toHtml()
    {
        return parent::_toHtml();
    }
	/**
     * Get key pieces for caching block content
     *
     * @return array
    */
	public function getCacheKeyInfo()
    {
        return array_merge(parent::getCacheKeyInfo(), array(
            $this->getDisplayType()
        ));
    }
    /**
     * Retrieve display type for products
     *
     * @return string
    */	
	 public function getDisplayType()
    {
        if (!$this->hasData('display_type')) {
            $this->setData('display_type', self::DISPLAY_TYPE_VIEWED_PRODUCTS);
        }
        return $this->getData('display_type');
    }
	
	public function getProductsCount()
    {
        if (!$this->hasData('products_count')) {
            return parent::getProductsCount();
        }
        return $this->getData('products_count');
    }
	
	public function getPriceHtml($product)
	{
		$this->setTemplate('catalog/product/price.phtml');
		$this->setProduct($product);
		return $this->toHtml();
	}
	
	public function getAddToCartUrl($product) {
		return $this->helper('checkout/cart')->getAddUrl($product);
	}
	
	public function getAddToCompareUrl($product) {
		return $this->helper('catalog/product_compare')->getAddUrl($product);
	}
	
	public function getReviewsSummaryHtml($product) {
		return $this->getLayout()->createBlock('review/helper')->getSummaryHtml($product, 'short', false);
	}
	
	public function getTypeCollection()
    {
        $storeId = Mage::app()->getStore()->getId();
		if ($this->getDisplayType() == self::DISPLAY_TYPE_VIEWED_PRODUCTS ) {
			$products = Mage::getResourceModel('reports/product_collection')
			->addOrderedQty()
			->addAttributeToSelect('*')
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
			->addViewsCount();
		}
		if ($this->getDisplayType() == self::DISPLAY_TYPE_SALE_PRODUCTS){
			$products = Mage::getResourceModel('reports/product_collection')
			->addAttributeToSort('created_at', 'desc')
			->addAttributeToSelect('*')
			->setStoreId($storeId)
			->addStoreFilter($storeId)
            ->addAttributeToFilter('special_price', array('gt' => 0))
			->addAttributeToFilter(array(
					array('attribute' => 'special_from_date', 'lt' => new Zend_Db_Expr('NOW()')),
					array('attribute' => 'special_from_date', 'null' => '')
				), null, 'left')
					->addAttributeToFilter(array(
					array('attribute' => 'special_to_date', 'gt' => new Zend_Db_Expr('NOW()')),
					array('attribute' => 'special_to_date', 'null' => '')
				), null, 'left');
		} 
		if ($this->getDisplayType() == self::DISPLAY_TYPE_BEST_PRODUCTS) {
			$products = Mage::getResourceModel('customproducts/product_collection')
			->addOrderedQty()
			->addAttributeToSelect('*')
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
			->setOrder('ordered_qty', 'desc');
		}


		Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
		Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
        $products->setPageSize($this->getProductsCount())->setCurPage(1);
        return $products;
    }
}
/*
$review = Mage::getModel('review/review');
	$products = $review->getProductCollection();
	$products
		->addAttributeToSelect('*')
		->setStoreId($storeId)
		->addStoreFilter($storeId)
		->getSelect();
	$review->appendSummary($products);
*/
?>