<?php 

class Tm_Instagram_Helper_Data extends Mage_Core_Helper_Data
{

	const XML_PATH_CLIENT_ID	 = 'instagram/general/client_id';

	const XML_PATH_USER_ENABLED  = 'instagram/instagram_user/active';
	const XML_PATH_USER_USERNAME = 'instagram/instagram_user/username';
	const XML_PATH_COUNT_IMAGE	 = 'instagram/instagram_user/count';
	const XML_PATH_IMAGE_SIZE	 = 'instagram/instagram_user/image_size';
	const XML_PATH_IMAGE_COLUMNS = 'instagram/instagram_user/columns';
	const XML_PATH_VERT_SPACE	 = 'instagram/instagram_user/vertical_space';
	const XML_PATH_HOR_SPACE	 = 'instagram/instagram_user/horizontal_space';

	const XML_PATH_TAG_ENABLED	   	 = 'instagram/instagram_tag/active';
	const XML_PATH_TAG_SAMPLE_TAG  	 = 'instagram/instagram_tag/sample_tag';
	const XML_PATH_COUNT_IMAGE_TAG 	 = 'instagram/instagram_tag/count';
	const XML_PATH_IMAGE_SIZE_TAG 	 = 'instagram/instagram_tag/image_size';
	const XML_PATH_IMAGE_COLUMNS_TAG = 'instagram/instagram_tag/columns';

	private function getStore()
	{
		return Mage::app()->getStore();
	}

	private function config($code, $store = null){
		return Mage::getStoreConfig($code, $store);
	}

	public function isEnabledUser($store = null){
		return $this->config(self::XML_PATH_USER_ENABLED, $store);
	}

	public function isEnabledTag($store = null){
		return $this->config(self::XML_PATH_TAG_ENABLED, $store);
	}

	public function getClientID($store = null){
		return $this->config(self::XML_PATH_CLIENT_ID, $store);
	}

	public function getUserUsername($store = null){
		return $this->config(self::XML_PATH_USER_USERNAME, $store);
	}

	public function getCountImages($store = null){
		return $this->config(self::XML_PATH_COUNT_IMAGE, $store);
	}
	public function getCountImagesTag($store = null){
		return $this->config(self::XML_PATH_COUNT_IMAGE_TAG, $store);
	}

	public function getImageSize($store = null){
		return $this->config(self::XML_PATH_IMAGE_SIZE, $store);
	}
	public function getImageSizeTag($store = null){
		return $this->config(self::XML_PATH_IMAGE_SIZE_TAG, $store);
	}

	public function getImageColumns($store = null){
		return $this->config(self::XML_PATH_IMAGE_COLUMNS, $store);
	}

	public function getImageColumnsTag($store = null){
		return $this->config(self::XML_PATH_IMAGE_COLUMNS_TAG, $store);
	}

	public function getSampleTag($store = null){
		return $this->config(self::XML_PATH_TAG_SAMPLE_TAG, $store);
	}

	

	public function getSpacing($store = null){
		$spasing['vert'] = $this->config(self::XML_PATH_VERT_SPACE, $store);
		$spasing['horiz']  = $this->config(self::XML_PATH_HOR_SPACE, $store);
		return $spasing;
	}

	public function productTag()
	{
		$product_id 			= Mage::registry('current_product')->getId();
		$product 				= Mage::getModel('catalog/product')->load($product_id);
		$product_name 			= strtolower(str_replace(' ', '', $product->getName()));
		$product_sku 			= $product->getSku();
		$store_title 			= strtolower(str_replace(' ', '', Mage::getStoreConfig('design/head/default_title', Mage::app()->getStore())));
		$sample_tag 			= Mage::getStoreConfig('instagram/instagram_tag/sample_tag', Mage::app()->getStore());
		$product_tag_option 	= explode(',', Mage::getStoreConfig('instagram/instagram_tag/product_tag', Mage::app()->getStore()));

		if (gettype($product_tag_option) == 'string') {
			$product_tag = $sample_tag;
		} else {
			if (in_array('store_title', $product_tag_option)) {
			$product_tag .= $store_title;
			} 
			if (in_array('product_name', $product_tag_option)){
				$product_tag .= $product_name;
			} 
			if (in_array('product_sku', $product_tag_option)){
				$product_tag .= $product_sku;
			} 
		}
		
		return $product_tag;
	}
}