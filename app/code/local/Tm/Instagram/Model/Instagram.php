<?php 

class Tm_Instagram_Model_Instagram
{
	public $username;
	public $client_id;
	public $tag;

	function __construct($params)
	{
		if(isset($params['username']))$this->username = $params['username'];
		if(isset($params['client_id']))$this->client_id = $params['client_id'];
		if(isset($params['tag'])) $this->tag = $params['tag'];
	}

	private function getHelper(){
		return Mage::helper('instagram/data');
	}

	private function fileGetContentsCurl($url) {
	   $ch = curl_init();
	   
	   curl_setopt($ch, CURLOPT_HEADER, 0);
	   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
	   curl_setopt($ch, CURLOPT_URL, $url);
	   
	   $data = curl_exec($ch);
	   curl_close($ch);
	   
	   return $data;
	}

	public function getUserId()
	{	
		$user_json = $this->fileGetContentsCurl('https://api.instagram.com/v1/users/search?q='. $this->username .'&client_id='. $this->client_id);
		$user_array = json_decode($user_json, true);
		$user_data = $user_array['data']['0'];

		return $user_data['id'];
	}

	public function userMediaRecent()
	{

		$media_json = $this->fileGetContentsCurl('https://api.instagram.com/v1/users/'. $this->getUserId() .'/media/recent/?client_id='. $this->client_id.'&count='.$this->getHelper()->getCountImages());
		$media_array = json_decode($media_json, true);

		//var_dump($media_array['data'][0]['images']['thumbnail']);
		$media_array['data'][0]['images']['low_resolution']['width']  = $this->getHelper()->getImageSize();
		$media_array['data'][0]['images']['low_resolution']['height'] = $this->getHelper()->getImageSize();

		$media_items = '';
		foreach ($media_array['data'] as $items) {
			$media_items[] = $items;
		}
		
		return $media_items;
	}

	public function tagMediaRecent()
	{
		$media_json = $this->fileGetContentsCurl('https://api.instagram.com/v1/tags/'. $this->tag .'/media/recent/?client_id='. $this->client_id.'&count='.$this->getHelper()->getCountImagesTag());
		$media_array = json_decode($media_json, true);

		$media_items = '';
		foreach ($media_array['data'] as $items) {
			$media_items[] = $items;
		}
		
		return $media_items;
	}
}