<?php

class Tm_StoreSettings_Model_Source_Types_Contacttypes
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'DESIND1', 'label'=>Mage::helper('adminhtml')->__('Layout 1')),
            array('value' => 'DESIND2', 'label'=>Mage::helper('adminhtml')->__('Layout 2'))
        );
    }
}