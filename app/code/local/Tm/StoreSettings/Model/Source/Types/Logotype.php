<?php 
class Tm_StoreSettings_Model_Source_Types_Logotype
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'logo_images', 'label'=>Mage::helper('adminhtml')->__('Images Logo')),
            array('value' => 'logo_text', 'label'=>Mage::helper('adminhtml')->__('Text Logo'))
        );
    }
}
?>