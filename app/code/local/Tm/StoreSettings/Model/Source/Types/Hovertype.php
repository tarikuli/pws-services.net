<?php 
class Tm_StoreSettings_Model_Source_Types_Hovertype
{
    public function toOptionArray()
    {
        return array(
			array('value' => 'disable_hover', 'label'=>Mage::helper('adminhtml')->__('Disable')),
            array('value' => 'thumbnails', 'label'=>Mage::helper('adminhtml')->__('Thumbnail')),
            array('value' => 'hover_images', 'label'=>Mage::helper('adminhtml')->__('Hover Images')),
			array('value' => 'carusel_images', 'label'=>Mage::helper('adminhtml')->__('Carousel Images'))
        );
    }
}
?>