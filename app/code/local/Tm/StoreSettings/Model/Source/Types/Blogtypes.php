<?php

class Tm_StoreSettings_Model_Source_Types_Blogtypes
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'BLOG_1', 'label'=>Mage::helper('adminhtml')->__('Layout 1')),
            array('value' => 'BLOG_2', 'label'=>Mage::helper('adminhtml')->__('Layout 2')),
			array('value' => 'BLOG_3', 'label'=>Mage::helper('adminhtml')->__('Layout 3')),
			array('value' => 'BLOG_4', 'label'=>Mage::helper('adminhtml')->__('Layout 4'))
        );
    }
}