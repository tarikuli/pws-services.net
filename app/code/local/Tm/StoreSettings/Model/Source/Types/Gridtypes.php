<?php

class Tm_StoreSettings_Model_Source_Types_Gridtypes
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'GRID_1', 'label'=>Mage::helper('adminhtml')->__('Layout 1')),
            array('value' => 'GRID_2', 'label'=>Mage::helper('adminhtml')->__('Layout 2')),
			array('value' => 'GRID_3', 'label'=>Mage::helper('adminhtml')->__('Layout 3')),
			array('value' => 'GRID_4', 'label'=>Mage::helper('adminhtml')->__('Layout 4'))
        );
    }
}