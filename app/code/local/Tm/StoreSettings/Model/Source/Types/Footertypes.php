<?php

class Tm_StoreSettings_Model_Source_Types_Footertypes
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'FOOTER_1', 'label'=>Mage::helper('adminhtml')->__('Layout 1')),
            array('value' => 'FOOTER_2', 'label'=>Mage::helper('adminhtml')->__('Layout 2')),
			array('value' => 'FOOTER_3', 'label'=>Mage::helper('adminhtml')->__('Layout 3'))
        );
    }
}