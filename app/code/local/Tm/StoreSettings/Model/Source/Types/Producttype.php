<?php 
class Tm_StoreSettings_Model_Source_Types_Producttype
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'layout_1', 'label'=>Mage::helper('adminhtml')->__('Layout 1')),
            array('value' => 'layout_2', 'label'=>Mage::helper('adminhtml')->__('Layout 2')),
			array('value' => 'layout_3', 'label'=>Mage::helper('adminhtml')->__('Layout 3'))
        );
    }
}
?>