<?php 

class Tm_StoreSettings_Helper_Data extends Mage_Core_Helper_Data
{

	public function getCfgGroup($group, $storeId = NULL)
    {
		if ($storeId)
			return Mage::getStoreConfig('storesettings/' . $group, $storeId);
		else
			return Mage::getStoreConfig('storesettings/' . $group);
    }
	
	public function getCfg($optionString, $storeCode = NULL)
    {
        return Mage::getStoreConfig('storesettings/' . $optionString, $storeCode);
    }
	
	/**
	 * Deprecated: old methods - for backward compatibility
	 */
	public function getCfgOld($optionString, $storeCode = NULL)
    {
        return $this->getCfg($optionString);
    }
	/**
	 * Seo
	 */
	public function getSeoOffer()
	{
		return 'itemscope itemtype="://schema.org/Offer"';
	} 
	public function getSeoPrice()
	{
		return 'itemprop="price"';
	} 
	public function getSeoName()
	{
		return 'itemprop="name"';
	} 
	public function getSeoImage()
	{
		return 'itemprop="image"';
	} 
	public function getSeoUrl()
	{
		return 'itemprop="url"';
	} 
	public function getSeoDesc()
	{
		return 'itemprop="description"';
	} 
	public function getSeoAvailability()
	{
		return 'itemprop="availability"';
	}
	public function getSeoProduct()
	{
		return 'itemscope itemtype="//schema.org/product"';
	} 
	public function getSeoAvail()
	{
		return 'itemprop="availability"';
	} 	
	
	
	/* public function getFooterLayouts()
	{
		return Mage::getModel('storesettings/source_types_footertypes')->toOptionArray();
	}
	
	public function getCurrentFooterLayout(){
		return $this->getCfg('footer_set/type');
	}
	
	public function renderSelectLayouts(){
		$select = '<select name="test">';
			foreach ($this->getFooterLayouts() as $key){
				if($key['value'] == $this->getCurrentFooterLayout()){
					$selected = 'selected="selected"';
				}else{
					$selected = '';
				}
					$select  .='<option value="'.$key['value'].'" '.$selected.'>'.$key['label'].'</option>';
			}
		$select .= '</select>';
		
		return $select;
	}
	
	public function initCookie()
	{
		return Mage::getModel('core/cookie');
	}
	
	public function setCookie($value)
	{
		$this->initCookie()->delete('mycookie');
		return $this->initCookie()->set('mycookie', $value ,time()+3600,'/');
	}
	
	public function getCookie($name)
	{
		
		return $this->initCookie()->get($name);
	}
	
	
	public function getPostArray()
	{	
		if(!isset($_GET['test'])) return;
		return $_GET['test'];
	}
    public function getCategoryIdByIdPath($idPath)
    {
        $storeId_2 = Mage::app()
            ->getWebsite()
            ->getDefaultGroup()
            ->getDefaultStoreId();

        $rewrite = Mage::getResourceModel('catalog/url')->getRewriteByIdPath($idPath, $storeId_2);
        $categoryId = $rewrite->getCategoryId();

        return $categoryId;
    } */

}
?>