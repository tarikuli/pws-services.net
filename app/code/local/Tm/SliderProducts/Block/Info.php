<?php

class Tm_SliderProducts_Block_Info
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{
   protected function _construct()
    {
       parent::_construct();
    }

    protected function _toHtml()
    {
        return parent::_toHtml();
    }
	/* protected function _prepareLayout() 
	{
		if ($head = $this->getLayout()->getBlock('head')) { 
			$head->addItem('skin_js', 'js/camera.js');
		}
	} */
	
	public function getProductsCount()
    {
        if (!$this->hasData('products_count')) {
            return parent::getProductsCount();
        }
        return $this->getData('products_count');
    }
	
	public function getPriceHtml($product)
	{
		$this->setTemplate('catalog/product/price.phtml');
		$this->setProduct($product);
		return $this->toHtml();
	}
	public function getAddToCartUrl($product) {
		return $this->helper('checkout/cart')->getAddUrl($product);
	}
	
	public function getSliderCollection()
    {
        $storeId = Mage::app()->getStore()->getId();
        $products = Mage::getResourceModel('catalog/product_collection')
			->setOrder('updated_at', 'desc')
			->addAttributeToSelect(array('name', 'price', 'small_image', 'short_description', 'slider', 'thumbnail', 'media_gallery'))
			->addAttributeToFilter('slider',array('eq'=>'1'))
            ->setStoreId($storeId)
            ->addStoreFilter($storeId);
        //Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        //Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
        $products->setPageSize($this->getProductsCount())->setCurPage(1);
        return $products;
    }

}