<?php

class Tm_SliderProducts_Model_Source_Types_Transition
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'fade', 'label'=>Mage::helper('adminhtml')->__('Fade')),
            array('value' => 'backSlide', 'label'=>Mage::helper('adminhtml')->__('BackSlide')),
			array('value' => 'goDown', 'label'=>Mage::helper('adminhtml')->__('GoDown')),
			array('value' => 'fadeUp', 'label'=>Mage::helper('adminhtml')->__('FadeUp')),
			array('value' => 'false', 'label'=>Mage::helper('adminhtml')->__('Swipe'))
        );
    }
}