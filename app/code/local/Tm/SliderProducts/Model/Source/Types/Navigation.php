<?php

class Tm_SliderProducts_Model_Source_Types_Navigation
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'false', 'label'=>Mage::helper('adminhtml')->__('No')),
            array('value' => 'true', 'label'=>Mage::helper('adminhtml')->__('Yes'))
        );
    }
}