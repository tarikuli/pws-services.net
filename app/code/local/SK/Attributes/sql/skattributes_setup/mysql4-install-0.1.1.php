<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('salesrule'),
        'coupon_purchase_price',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
            'length'    => '10,2',
            'nullable' => false,
            'default'   => 0.00,
            'comment' => 'coupon_purchase_price'
        )
    );

$installer->endSetup();