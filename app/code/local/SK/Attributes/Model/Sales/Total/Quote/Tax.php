<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Tax
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Tax totals calculation model
 */
class SK_Attributes_Model_Sales_Total_Quote_Tax extends Mage_Tax_Model_Sales_Total_Quote_Tax
{

 public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $applied = $address->getAppliedTaxes();
        $store = $address->getQuote()->getStore();
        $amount = $address->getTaxAmount();

        $items = $this->_getAddressItems($address);
        $discountTaxCompensation = 0;
        foreach ($items as $item) {
            $discountTaxCompensation += $item->getDiscountTaxCompensation();
        }
        $taxAmount = $amount + $discountTaxCompensation;
        /*
         * when weee discount is not included in extraTaxAmount, we need to add it to the total tax
         */
        if ($this->_weeeHelper->isEnabled()) {
            if (!$this->_weeeHelper->includeInSubtotal()) {
                $taxAmount += $address->getWeeeDiscount();
            }
        }

        $area = null;
        if ($this->_config->displayCartTaxWithGrandTotal($store) && $address->getGrandTotal()) {
            $area = 'taxes';
        }

        if (($amount != 0) || ($this->_config->displayCartZeroTax($store))) {
             $coupon_code = Mage::getSingleton('checkout/session')->getQuote()->getCouponCode();
                if(!empty($coupon_code)){
                    $oCoupon = Mage::getModel('salesrule/coupon')->load($coupon_code, 'code');
                    $oRule = Mage::getModel('salesrule/rule')->load($oCoupon->getRuleId());
                    $store = Mage::app()->getStore(); // store info
                   $getConfiP = Mage::getStoreConfig('inchoo/inchoo_group/inchoo_input', $store);
                   $costPrice = $oRule->getData('coupon_purchase_price');
                    $finalCtaxPrice  = $costPrice*$getConfiP;
                }
                $setPrice = 0;
                if($finalCtaxPrice){
                    $setPrice = $finalCtaxPrice;
                }

            if(!empty($coupon_code)){
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => Mage::helper('tax')->__('Tax<font color="red" size="2px"> (Includes Tax On Coupon Purchase Price</font>)'),
                'full_info' => $applied ? $applied : array(),
                'value' => $amount+$setPrice,
                'area' => $area
            ));
        } else {

            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => Mage::helper('tax')->__('Tax'),
                 'full_info' => $applied ? $applied : array(),
                'value' => $amount+$setPrice,
                'area' => $area
            ));
           }
        }
        $store = $address->getQuote()->getStore();
        /**
         * Modify subtotal
         */
        if ($this->_config->displayCartSubtotalBoth($store) || $this->_config->displayCartSubtotalInclTax($store)) {
            if ($address->getSubtotalInclTax() > 0) {
                $subtotalInclTax = $address->getSubtotalInclTax();
            } else {
                $subtotalInclTax = $address->getSubtotal() + $taxAmount - $address->getShippingTaxAmount();
            }

            $address->addTotal(array(
                'code' => 'subtotal',
                'title' => Mage::helper('sales')->__('Subtotal'),
                'value' => $subtotalInclTax,
                'value_incl_tax' => $subtotalInclTax,
                'value_excl_tax' => $address->getSubtotal(),
            ));
        }
        return $this;
    }

}
