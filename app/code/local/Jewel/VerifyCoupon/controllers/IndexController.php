<?php
class Jewel_VerifyCoupon_IndexController extends Mage_Core_Controller_Front_Action
{
    #<args>
    #   <module>Jewel_VerifyCoupon</module>
    #   <frontName>coupon</frontName>
    #</args>
    # // https://www.mymonogramonline.com/coupon/index/index
    public function indexAction()
    {
    	$this->loadLayout();
//     	Mage::log('indexAction '.date('Y-m-d H:i:s').print_r($this->loadLayout(), true), null, 'system.log');
        # Add Head titel.
    	$this->getLayout()
            ->getBlock("head")
            ->setTitle($this->__("Verify Voucher"));
        
        # Call Home breadcrumbs block
        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");

        # Put Home icon in breadcrumbs
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home Page"),
            "title" => $this->__("Home Page"),
            "link" => Mage::getBaseUrl()
        ));

        # Add Text breadcrumbs
        $breadcrumbs->addCrumb("verify voucher", array(
            "label" => $this->__("Verify voucher"),
            "title" => $this->__("Verify voucher")
        ));

        $this->renderLayout();
    }
}