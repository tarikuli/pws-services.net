<?php

class Jewel_VerifyCoupon_Block_Form extends Mage_Core_Block_Template
{
	public function getCouponCode()
	{
		$couponCode = $this->getRequest()->getParam('coupon_code');

		if(!isset($couponCode))
		{
			$couponCode = Mage::getSingleton('core/session')->getCouponCode();

			if(isset($couponCode))
				return $couponCode;
		}
		else
		{
			Mage::getSingleton('core/session')->setCouponCode($couponCode);
			return $couponCode;
		}

		return '';
	}

	public function getCouponDescription()
	{
        $couponCode = $this->getCouponCode();

		if($couponCode)
		{
		    // Call
		    $verifyVoucher = Mage::helper('jewel_verifycoupon');
			$oCoupon = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');

			$couponData = $oCoupon->getData();

			$oRule = Mage::getModel('salesrule/rule')->load($oCoupon->getRuleId());

			if ((!$oCoupon->getRuleId()) || (!$oRule->getIsActive()))
			{
				return "<p><strong>Sorry but we can't seem to find this code in our database. </strong></p><p>Please make sure that the code has been entered correctly. </p> <br><p>Please note : All promotional coupon /voucher codes start with es, gn, gg, g5, g4, or g2 etc.</p><p>The information starting with '1000-' or 'LG-' is receipt # for your purchase of the code and</p><p>cannot be applied as coupon.</p><p>Please refer to your Social Deal Site for the correct coupon /voucher code</p>";
			} elseif ($couponData['times_used'] == 1) {
			    
				$orderCollection = Mage::getModel('sales/order')->getCollection()
					->addAttributeToSelect('increment_id')
					->addAttributeToSelect('customer_firstname')
					->addAttributeToSelect('customer_lastname')
					->addAttributeToSelect('customer_email')
					->addAttributeToSelect('created_at')
					->addAttributeToFilter('coupon_code', $couponCode);

            	$orderInfo = $orderCollection->getData();

            	$couponMessage = $this->getStoreLabels($oCoupon->getRuleId());

//             	echo "<pre>"; 
//             	print_r($orderInfo); 
//             	echo "--------------------------------------------";
//             	print_r($couponData);
//             	echo "--------------------------------------------";
//             	print_r($oRule->getData());
//             	echo "--------------------------------------------";
//             	echo "</pre>"; 
//             	die();
            	
            	
            	if(isset($orderInfo[0]['created_at']))
            	{
            	    //$verifyVoucher->getVoucherLabels($oRule);
            	    
//             	    return '<p><strong>**We found your code in the our database but it was already redeemed on : '. Mage::getModel('core/date')->date('M d, Y', $orderInfo[0]['created_at']).', Order# '.$orderInfo[0]['increment_id'].'</strong>
//                             <br><b>Coupon Description:</b> '.$oRule->getDescription().'<br>
//                             <b>Coupon Value:</b> '.$oRule->getSimpleAction().', '. $oRule->getDiscountAmount().'
//                             </p>';
                
            	    return '<p><strong>We found your code in the our database but it was already redeemed on : '. Mage::getModel('core/date')->date('M d, Y', $orderInfo[0]['created_at']).', Order# '.$orderInfo[0]['increment_id'].'</strong>
                            '.$verifyVoucher->getVoucherLabels($oRule).'
                             <br><b>Your coupon is valid for:</b> '.$this->getStoreLabels($oCoupon->getRuleId()).' From the following collection
                            </p>';
            	} else {
            		return '<p><strong>We found your code in the our database but it was already redeemed.<strong></p><p>Your coupon was valid for : '. $couponMessage .' From the following collection</p>';
            	}
			}
			else
			{
				if (count($couponData) > 0)
				{
					$couponMessage = $this->getStoreLabels($oCoupon->getRuleId());

					return "<p><strong>Yay! We found your voucher in our system</strong><p>".$verifyVoucher->getVoucherLabels($oRule)."<p><b>Your coupon is valid for :</b> ".$couponMessage." From the following collection</p><br><p>Important Note:</p><p>When Processing a voucher code for MULTIPLE ITEMS it is necessary to click on the 'Keep shopping' button<p><p>in the shopping cart to enter the additional designs/personalization<p>";
				}
				else
				{
               		return '<p>Please accept our apologies but the promotional codes (redemption certificates) start with ck, es, gn, gg, g5, g4, or g2 etc.<p> <p>The information provided that starts with 1000- OR LG- is the receipt for your purchase of the discount code which will not work in our system.<p>';
            	}
			}
		}

		return null;
	}

	public function getStoreLabels($ruleId)
    {
        $resource = Mage::getSingleton('core/resource');

        $readConnection = $resource->getConnection('core_read');

        $query = "SELECT label FROM " . $resource->getTableName('salesrule/label')." where rule_id = '".$ruleId."' LIMIT 1";

        $results = $readConnection->fetchAll($query);

        if(isset($results[0]))
            return $results[0]['label'];

        return null;
    }
}