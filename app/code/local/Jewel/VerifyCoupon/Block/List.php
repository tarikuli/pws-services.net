<?php

class Jewel_VerifyCoupon_Block_List extends Mage_Catalog_Block_Product_List
{
    protected function _getProductCollection()
    {
        if(is_null($this->_productCollection))
        {
            $collection = Mage::getResourceModel('catalog/product_collection')
	            ->setStoreId($this->getStoreId());

            $collection->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
	            ->addMinimalPrice()
	            ->addFinalPrice()
	            ->addTaxPercents();

	        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
	        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

	        $skus = $this->getProductSkus();

	        $collection->addAttributeToFilter('sku', array('in' => $skus));

	        $this->_productCollection = $collection;

	        // $this->_productCollection = parent::_getProductCollection();

			// $skus = $this->getProductSkus();

         	//$this->_productCollection->addAttributeToFilter('sku', array('in' => $skus));
        }

        return $this->_productCollection;
    }

    public function getStoreId()
    {
        return Mage::app()->getStore()->getId();
    }

    public function getCouponCode()
	{
		$couponCode = $this->getRequest()->getParam('coupon_code');

		if(!isset($couponCode))
		{
			$couponCode = Mage::getSingleton('core/session')->getCouponCode();

			if(isset($couponCode))
				return $couponCode;
		}
		else
		{
			Mage::getSingleton('core/session')->setCouponCode($couponCode);
			return $couponCode;
		}

		return '';
	}

	public function getProductSkus()
	{
        $couponCode = $this->getCouponCode();

		if($couponCode)
		{
			$oCoupon = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');

			$couponData = $oCoupon->getData();

			$oRule = Mage::getModel('salesrule/rule')->load($oCoupon->getRuleId());

			$rule_arr = unserialize($oRule->getConditionsSerialized());

        	$conditions_arr = isset($rule_arr['conditions'])? $rule_arr['conditions']: array();

        	foreach ($conditions_arr as $con_key => $con_value)
        	{
                if(isset($con_value['conditions']))
                {
                    foreach ($con_value['conditions'] as $condition_key => $condition_value)
                    {
                        if ($condition_value['attribute'] == "sku" && isset($con_value['conditions']) && isset($condition_value['value']))
                        {
                            return explode(',', str_replace(' ', '', $condition_value['value']));
                        }
                    }
                }
            }
		}

		return array();
	}
}
