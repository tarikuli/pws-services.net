<?php

class Jewel_VerifyCoupon_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getVoucherLabels($oRule)
    {
//         return '<br><b>Coupon Description:</b> '.$oRule->getDescription().'<br>
//                 <b>Coupon Value:</b> '. $this->actionType($oRule->getSimpleAction()).' $'. $oRule->getDiscountAmount();
        
        return '<br><b>Coupon Value:</b> '. $this->actionType($oRule->getSimpleAction()).' $'. $oRule->getDiscountAmount();
        
    }
    
    private function actionType($actionName){
        
        $actionArray['by_percent'] = "Percent of product price discount";
        $actionArray['by_fixed'] = "Fixed amount discount";
        $actionArray['cart_fixed'] = "Fixed amount discount for whole cart";
        $actionArray['buy_x_get_y'] = "Buy X get Y free (discount amount is Y)";
        $actionArray['ampromo_cart'] = "Auto add promo items for the whole cart";
        $actionArray['ampro mo_items'] = "Auto add promo items with products";
        $actionArray['ampromo_product'] = "Auto add the same product";
        $actionArray['ampromo_spent'] = "Auto add promo items for every spent";
        
        if(isset($actionArray[$actionName])){
            return $actionArray[$actionName].", ";
        }else{
            return " ";
        }
        
    }
    
}