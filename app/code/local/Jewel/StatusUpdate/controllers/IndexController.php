<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Jewel_StatusUpdate_IndexController extends Mage_Core_Controller_Front_Action
{

    private $client;

    private $sessionId;

    protected $_orderStateArray = ["new", "pending_payment", "processing", "complete", "closed", "canceled", "holded", "payment_review"];

    protected $_orderStatusArray = ["pending_processing", "processing", "order_printed", "order_qc", "order_shipped", "unverifiable_address", "holded","canceled"];

    // http://www.pws-services.net/statusupdate?apiUser=collage&apiPass=Collage99%7E%21&orderId=1216903&orderState=processing&orderStatus=pending_processing&orderComment=Changing+state+to+Processing+and+status+to+expedier+Status
    public function IndexAction()
    {
        $oSD = $this->getRequest()->getParams();
        $wsdlUrl = 'http://www.pws-services.net/api/v2_soap?wsdl=1';
        $this->client = new SoapClient($wsdlUrl);
        $this->sessionId = $this->client->login($oSD["apiUser"], $oSD["apiPass"]);

        if (!$this->sessionId) {
            echo "<pre>";
            echo json_encode([
                'result' => "You don't have permission",
                'status' => "error"
            ]);
            echo "</pre>";
            exit();
        }

        if(isset($oSD["addTracking"])){
            $this->addTracking($oSD);
            exit();
        }

        if(isset($oSD["pushOrderId"])){
            $this->pushOrderOms($oSD["pushOrderId"]);
            exit();
        }

        if (!in_array($oSD["orderState"], $this->_orderStateArray)) {
            echo "<pre>";
            echo json_encode([
                'expected_parameter' => $this->_orderStateArray,
                'result' => "Order State not found",
                'status' => "error"
            ]);
            echo "</pre>";
            exit();
        }

        if (!in_array($oSD["orderStatus"], $this->_orderStatusArray)) {
            echo "<pre>";
            echo json_encode([
                'expected_parameter' => $this->_orderStatusArray,
                'result' => "Order Status not found",
                'status' => "error"
            ]);
            echo "</pre>";
            exit();
        }

        if (!isset($oSD["orderComment"])) {
            echo "<pre>";
            echo json_encode([
                'result' => "Order Comment not found",
                'status' => "error"
            ]);
            echo "</pre>";
            exit();
        }

        if (isset($oSD["orderId"])) {
            $orderIncrementId = $oSD["orderId"];
            $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);

            if ($order->getStatus()) {

                if ($oSD["orderState"] == "canceled") {
                    if ($order->canCancel()) {
                        $order->cancel()->save();
                        echo "<pre>" . json_encode(['order_id' => $orderIncrementId, 'result' => "Order Cancel.", 'status' => "success"]) . "</pre>";
                        exit();
                    } else {
                        echo "<pre>";
                        echo json_encode(['result' => "Order can't Cancel.", 'status' => "error"]);
                        echo "</pre>";
                        exit();
                    }
                }

                if ($oSD["orderState"] == "holded") {
                    if ($order->canHold()) {
                        $order->hold()->save();
                        echo "<pre>" . json_encode(['order_id' => $orderIncrementId, 'result' => "Order hold.", 'status' => "success"]) . "</pre>";
                        exit();
                    } else {
                        echo "<pre>";
                        echo json_encode(['result' => "Order can't Hold.", 'status' => "error"]);
                        echo "</pre>";
                        exit();
                    }
                }

                $isCustomerNotified = true;
                $order->setState($oSD["orderState"], $oSD["orderStatus"], $oSD["orderComment"], $isCustomerNotified);
                $order->save();

                echo "<pre>";
                echo json_encode([
                    'order_id' => $orderIncrementId,
                    'result' => "Order Status Updated.",
                    'status' => "success"
                ]);
                echo "</pre>";
                exit();
            } else {
                echo "<pre>";
                echo json_encode([
                    'order_id' => $orderIncrementId,
                    'result' => "Order Not found",
                    'status' => "error"
                ]);
                echo "</pre>";
            }


            // }
        } else {
            echo "<pre>";
            echo json_encode([
                'result' => "Order Id not set",
                'status' => "error"
            ]);
            echo "</pre>";
        }

    }

    public function addTracking($oSD){


        if (isset($oSD["orderId"])) {
            $orderIncrementId = $oSD["orderId"];
            $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);

            if ($order->getStatus()) {

                if($order->canShip())
                {
                    $itemQty =  $order->getItemsCollection()->count();
                    # Create Shipment
                    $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($itemQty);
                    $shipment = new Mage_Sales_Model_Order_Shipment_Api();
                    $shipmentId = $shipment->create($orderIncrementId);

                    ### Add Tracking Number ###
                    $trackmodel = Mage::getModel('sales/order_shipment_api')
                        ->addTrack($shipmentId,'custom',$oSD["carrierTitel"], $oSD["addTracking"]);

                    echo "<pre>";
                    echo json_encode([
                        'order_id' => $orderIncrementId,
                        'shipmentId' => $shipmentId,
                        'trackingNumber' => $oSD["addTracking"],
                        'result' => "Tracking Updated.",
                        'status' => "success"
                    ]);
                    echo "</pre>"; return;
                }else{
                    echo "<pre>";
                    echo json_encode([
                        'order_id' => $orderIncrementId,
                        'result' => "Can't create shipment",
                        'status' => "error"
                    ]);
                    echo "</pre>"; return;
                }

                echo "<pre>";
                echo json_encode([
                    'result' => $oSD,
                    'status' => "error"
                ]);
                echo "</pre>"; return;
            }else {
                echo "<pre>";
                echo json_encode([
                    'order_id' => $orderIncrementId,
                    'result' => "Order Not found",
                    'status' => "error"
                ]);
                echo "</pre>"; return;
            }
        } else {
            echo "<pre>";
            echo json_encode([
                'result' => "Order Id not set",
                'status' => "error"
            ]);
            echo "</pre>"; return;
        }
    }

    public function pushOrderOms($orderId){
        try {
            $orderObject = Mage::getModel('sales/order')->loadByIncrementId($orderId);

            if (!$orderObject->getCustomerId()) {
                echo "<pre>";
                echo json_encode([
                    'result' => "The order # " . $orderId. " not exist",
                    'status' => "error"
                ]);
                echo "</pre>"; exit();
            }


            $apiHelper = Mage::helper('oms_api/oms');
            $apiHelper->getOmsUrl();

            if (Mage::registry('oms_array')) {
                $oms = Mage::registry('oms_array');
                Mage::unregister('oms_array');

                $apiHelper->pushPurchaseToOms(array($orderObject->getId()), $oms['storeID'], $oms['path'], $oms['omsUrl']);

                echo "<pre>";
                echo json_encode([
                    'result' => "The order# ".$orderId." Pushed to " . $oms['omsUrl'],
                    'status' => "success"
                ]);
                echo "</pre>"; exit();
            } else {
                echo "<pre>";
                echo json_encode([
                    'result' => 'No 6p Store found.',
                    'status' => "error"
                ]);
                echo "</pre>"; exit();
            }
        } catch (Mage_Core_Exception $e) {
            echo "<pre>";
            echo json_encode([
                'result' => $e->getMessage(),
                'status' => "error"
            ]);
            echo "</pre>"; exit();
        } catch (Exception $e) {
            echo "<pre>";
            echo json_encode([
                'result' => $e->getMessage(),
                'status' => "error"
            ]);
            echo "</pre>"; exit();
        }
    }

}