<?php

require_once(Mage::getModuleDir('controllers','Mage_Adminhtml') . DS . 'Promo' . DS . 'QuoteController.php');

class Jewel_CouponImport_Adminhtml_Promo_QuoteController extends Mage_Adminhtml_Promo_QuoteController
{
    protected $_deleteStatus = false;
    
    public function saveAction()
    {
        if ($this->getRequest()->getPost()) {
            try {
                /** @var $model Mage_SalesRule_Model_Rule */
                $model = Mage::getModel('salesrule/rule');
                Mage::dispatchEvent(
                    'adminhtml_controller_salesrule_prepare_save',
                    array('request' => $this->getRequest()));
                $data = $this->getRequest()->getPost();
                $data = $this->_filterDates($data, array('from_date', 'to_date'));
                $id = $this->getRequest()->getParam('rule_id');
                if ($id) {
                    $model->load($id);
                    if ($id != $model->getId()) {
                        Mage::throwException(Mage::helper('salesrule')->__('Wrong rule specified.'));
                    }
                }

                $session = Mage::getSingleton('adminhtml/session');

                $validateResult = $model->validateData(new Varien_Object($data));
                if ($validateResult !== true) {
                    foreach($validateResult as $errorMessage) {
                        $session->addError($errorMessage);
                    }
                    $session->setPageData($data);
                    $this->_redirect('*/*/edit', array('id'=>$model->getId()));
                    return;
                }

                if (isset($data['simple_action']) && $data['simple_action'] == 'by_percent'
                && isset($data['discount_amount'])) {
                    $data['discount_amount'] = min(100,$data['discount_amount']);
                }
                if (isset($data['rule']['conditions'])) {
                    $data['conditions'] = $data['rule']['conditions'];
                }
                if (isset($data['rule']['actions'])) {
                    $data['actions'] = $data['rule']['actions'];
                }
                unset($data['rule']);
                $model->loadPost($data);

                $useAutoGeneration = (int)!empty($data['use_auto_generation']);
                $model->setUseAutoGeneration($useAutoGeneration);

                $session->setPageData($model->getData());

                $model->save();

                $this->processUploadedFile($model);

                $session->addSuccess(Mage::helper('salesrule')->__('The rule has been saved.'));
                $session->setPageData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $id = (int)$this->getRequest()->getParam('rule_id');
                if (!empty($id)) {
                    $this->_redirect('*/*/edit', array('id' => $id));
                } else {
                    $this->_redirect('*/*/new');
                }
                return;

            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('catalogrule')->__('An error occurred while saving the rule data. Please review the log and try again.'));
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->setPageData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('rule_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    protected function removeExistingRule($model){
        
        $couponSalesrules = Mage::getModel('salesrule/coupon')
        ->getCollection()
        ->addFieldToFilter('rule_id', array($model->getRuleId()));
        
        foreach($couponSalesrules as $couponSalesrule) {
            $couponSalesrule->delete();
//             Mage::log('delete = '.print_r($couponSalesrule->getCode(), true), null, 'system.log', true);
        }
        
//         $model->setUseAutoGeneration(1)->save();
        return $this->_deleteStatus = true;
    }

    
    protected function processUploadedFile($model)
    {
        if(isset($_FILES['coupon_file']['name']) and (file_exists($_FILES['coupon_file']['tmp_name'])))
        {
            try
            {
                $uploader = new Varien_File_Uploader('coupon_file');
                $uploader->setAllowedExtensions(array('csv'));
                $uploader->setAllowRenameFiles(false);
                $path = Mage::getBaseDir('var') . DS ;
                $uploader->save($path, $_FILES['coupon_file']['name']);
                $file = $path . $uploader->getUploadedFilename();
                if(file_exists($file))
                {
                    set_time_limit(0);
                    $csvObject = new Varien_File_Csv();
                    $data = $csvObject->getData($file);

                    $now = $model->getResource()->formatDate(
                        Mage::getSingleton('core/date')->gmtTimestamp()
                    );

                    
                    $coupon = Mage::getModel('salesrule/coupon');
                    $count = 0;
                    
                    
                    foreach($data as $item)
                    {

                        if($count++ == 0 || !isset($item[0]) || trim($item[0]) == "")
                            continue;

                        $expirationDate = $model->getToDate();
                        if ($expirationDate instanceof Zend_Date) {
                            $expirationDate = $expirationDate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
                        }

                        try
                        {

                            
                            if(isset($item[0]) && isset($item[1]) && isset($item[2]) && $item[1] == $model->getName()){

                                if(!$this->_deleteStatus){
                                    $this->removeExistingRule($model);
                                }

// Mage::log($count.'  CSV = '.$item[0].' = '.$item[1].' = '.$item[2].' = '.$model->getName(), null, 'system.log', true);
                              $coupon->setId(null)
                                    ->setRuleId($model->getRuleId())
                                    ->setUsageLimit($model->getUsesPerCoupon())
                                    ->setUsagePerCustomer($model->getUsesPerCustomer())
                                    ->setTimesUsed($item[2] == 'redeemed' ? 1 : 0)
                                    ->setExpirationDate($expirationDate)
                                    ->setCreatedAt($now)
                                    ->setType(Mage_SalesRule_Helper_Coupon::COUPON_TYPE_SPECIFIC_AUTOGENERATED)
                                    ->setCode($item[0])
                                    ->save()
                                ;
                            }
                        }
                        catch(Exception $e) { }
                    }

                    unlink($file);
                }
            } catch(Exception $e) { }
        }
    }
}