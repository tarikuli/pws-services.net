<?php

class Jewel_Oms_Helper_Oms extends Mage_Core_Helper_Abstract
{

    public function pushPurchaseToOms($orderIds, $storeID, $storeName, $omsUrl)
    {
        Mage::log(print_r($orderIds, true), null, 'jewel_oms.log', true);
        // return false;
        foreach ($orderIds as $orderId) {
            try {
//            $orderObject = Mage::getModel('sales/order')->loadByIncrementId($orderId);
                $orderObject = Mage::getModel('sales/order')->load($orderId);

//                Mage::log(print_r($orderObject->debug(), true), null, 'jewel_oms.log', true);


                if ($orderObject->getCustomerId()) {
                    $customerObject = Mage::getModel('customer/customer')->load($orderObject->getCustomerId());
                    $billingAddress = $customerObject->getPrimaryBillingAddress();
                    $shippingAddress = $orderObject->getShippingAddress();
                } else {
                    Mage::log(print_r($orderObject->getCustomerId(), true), null, 'jewel_oms.log', true);
                    Mage::log('OMS Order_ID = (' . $orderId . ') not sent. ', null, 'jewel_oms.log');
                    return false;
                }


                $po_number = $orderObject->getPayment()->getPoNumber();
                if(!$po_number) {
                  $po_number = null;
                }


                $purchaseData['Bill-Address1'] = $billingAddress->getStreet1();
                $purchaseData['Bill-Address2'] = $billingAddress->getStreet2();
                $purchaseData['Bill-City'] = $billingAddress->getCity();
                $purchaseData['Bill-Company'] = '';
                $purchaseData['Bill-Country'] = $billingAddress->getCountryId();
                $purchaseData['Bill-Email'] = $customerObject->getEmail();
                $purchaseData['Bill-Firstname'] = $billingAddress->getFirstname();
                $purchaseData['Bill-Lastname'] = $billingAddress->getLastname();
                $purchaseData['Bill-Name'] = $billingAddress->getFirstname() . " " . $billingAddress->getLastname();
                $purchaseData['Bill-Phone'] = $billingAddress->getTelephone();
                $purchaseData['Bill-State'] = $billingAddress->getRegion();
                $purchaseData['Bill-Zip'] = $billingAddress->getPostcode();
                $purchaseData['Bill-maillist'] = 'no';
                $purchaseData['Card-Expiry'] = 'xx/xxxx';
                $purchaseData['Card-Name'] = 'PayPal';
                $purchaseData['Comments'] = $orderObject->getCustomerNote();
                $purchaseData['Coupon-Description'] = $orderObject->getGiftCodes() . " " . $orderObject->getCouponCode();
                $purchaseData['Coupon-Id'] = $orderObject->getGiftCodes() . " " . $orderObject->getCouponCode();
                // $purchaseData['Coupon-Value'] = $orderObject->getGiftVoucherDiscount();
                $purchaseData['Coupon-Value'] = abs($orderObject->getDiscountAmount());
                $purchaseData['Date'] = $orderObject->getCreatedAt();
                $purchaseData['ID'] = $storeID . '-' . $orderObject->getIncrementId();
                $purchaseData['Purchase-Order'] = $po_number;
                $purchaseData['IP'] = $orderObject->getRemoteIp();

                $orderItems = $orderObject->getAllItems();
                $shippingMethod = $orderObject->getShippingMethod(); // jewel_priorityshipping_priority
                $shippingprice = $this->calShippingCost($orderItems, $shippingMethod);

                $index = 1;
                foreach ($orderItems as $item) {
                    $productId = $item->getProductId();
                    $productObject = Mage::getModel('catalog/product')->load($productId);

                    $purchaseData['Item-Code-' . $index] = $item->getSku();
                    $purchaseData['Item-Description-' . $index] = $item->getName();
                    $purchaseData['Item-Id-' . $index] = $productObject->getUrlKey();
                    $purchaseData['Item-Quantity-' . $index] = $item->getQtyOrdered();
                    $purchaseData['Item-Taxable-' . $index] = 'Yes';
                    // $purchaseData['Item-Unit-Price-'.$index] = $item->getRowTotal();
                    $purchaseData['Item-Unit-Price-' . $index] = number_format(($item->getRowTotal() / $item->getQtyOrdered()), 2);
                    $purchaseData['Item-Url-' . $index] = "https://" . $storeName . "/" . $productObject->getUrlPath();
//                    $purchaseData['Item-Thumb-' . $index] = "<img border=0 width=70 height=70 src=http://pws-services.net/media/images/product.png>";
                    $purchaseData['Item-Thumb-' . $index] = "<img border=0 width=70 height=70 src=" . $productObject->getImageUrl() . ">";

                    // Another for loop for Parameter Options
                    $itemOptions = $item->getProductOptions();

                    if (isset($itemOptions['options'])) {
                        foreach ($itemOptions['options'] as $value) {
                            $purchaseData['Item-Option-' . $index . '-' . trim(str_replace(":", "", $value['label']))] = $value['print_value'];
                        }
                    }

                    if (isset($itemOptions['additional_options'])) {
                        foreach ($itemOptions['additional_options'] as $value) {
                            $purchaseData['Item-Option-' . $index . '-' . trim(str_replace(":", "", $value['label']))] = $value['value'];
                            // Mage::log('Item-Option-'.$index.'-'.trim(str_replace(":", "", $value['label'])).' === '.$value['value'], null, 'jewel_oms.log');
                        }
                    }
                    $index++;
                }

                $purchaseData['Item-Count'] = ($index - 1);
                $purchaseData['Numeric-Time'] = strtotime($orderObject->getCreatedAt());
                $purchaseData['PayPal-Address-Status'] = 'Confirmed';
                $purchaseData['PayPal-Auth'] = '8F4701569X6000947';
                $purchaseData['PayPal-Merchant-Email'] = 'pablo@dealtowin.com';
                $purchaseData['PayPal-Payer-Status'] = 'Unverified';
                $purchaseData['PayPal-TxID'] = '75692712YB5948433';

                $purchaseData['Ship-Address1'] = $shippingAddress->getStreet1();
                $purchaseData['Ship-Address2'] = $shippingAddress->getStreet2();
                $purchaseData['Ship-City'] = $shippingAddress->getCity();
                $purchaseData['Ship-Company'] = '';
                $purchaseData['Ship-Country'] = $shippingAddress->getCountryId();
                $purchaseData['Ship-Firstname'] = $shippingAddress->getFirstname();
                $purchaseData['Ship-Lastname'] = $shippingAddress->getLastname();
                $purchaseData['Ship-Name'] = $shippingAddress->getFirstname() . " " . $shippingAddress->getLastname();
                $purchaseData['Ship-Phone'] = $shippingAddress->getTelephone();
                $purchaseData['Ship-State'] = $shippingAddress->getRegion();
                $purchaseData['Ship-Zip'] = $shippingAddress->getPostcode();
                $purchaseData['Shipping'] = $shippingMethod; // ToDo
                $purchaseData['Shipping-Charge'] = $shippingprice; #$orderObject->getShippingAmount();
                $purchaseData['Space-Id'] = '';
                $purchaseData['Store-Id'] = $storeID;
                $purchaseData['Store-Name'] = $storeName;
                $purchaseData['Tax-Charge'] = $orderObject->getBaseTaxAmount();
                $purchaseData['Total'] = $orderObject->getGrandTotal();

                $client = new Zend_Http_Client($omsUrl);
                $client->setMethod(Zend_Http_Client::POST);
                $client->setParameterPost($purchaseData);
                $client->setConfig(array(
                    'timeout' => 30
                ));

                $json = json_decode($client->request()->getBody(), TRUE);

                if (!isset($json["message"])) {
                    Mage::log('Order#  ' . $orderObject->getIncrementId() . ' not insert in p6', null, 'jewel_oms.log');

                    $toName = "Momo Admin";
                    $toMail = [
                        "tarikuli@yahoo.com",
//                    "lana@monogramonline.com",
                    "shlomi@monogramonline.com",
//                    "cs@monogramonline.com",
//                    "jennifer@monogramonline.com",
                    ];

                    $vars = array(
                        'order_id' => $orderObject->getIncrementId(),
                    );

                    $templateId = 44;
                    $this->sendNotification($toMail, $vars, $templateId, $toName);
                } else {
                    Mage::log('OMS Order_ID = (' . $orderObject->getIncrementId() . ') sent. ', null, 'jewel_oms.log');

                    $orderObject->setShippingAmount($shippingprice);
                    $orderObject->setBaseShippingAmount($shippingprice);
                    $orderObject->setGrandTotal($orderObject->getGrandTotal() + $shippingprice); //adding shipping price to grand total
                    $orderObject->save();
                }
            } catch (Exception $e) {

                Mage::log('OMS Order push error = (' . $e->getMessage() . ') sent. ', null, 'jewel_oms.log');
            }

        }
    }

    public function sendNotification($toMailArray, $vars, $templateId, $toName)
    {
        foreach ($toMailArray as $email) {
            $mailTemplate = Mage::getModel('core/email_template');
            $translate = Mage::getSingleton('core/translate');
//             $templateId = 44;
            $template_collection = $mailTemplate->load($templateId);
            $template_data = $template_collection->getData();

            if (!empty($template_data)) {
                $mailSubject = "Transactional Emails";
                $from_email = 'cs@monogramonline.com';
                $from_name = 'Customer Service';
                $sender = array(
                    'name' => "Customer Service",
                    'email' => 'cs@monogramonline.com'
                );

                $storeId = Mage::app()->getStore()->getStoreId();

                $model = $mailTemplate->setReplyTo($sender['email'])->setTemplateSubject($mailSubject);
                // $email = 'jewel@monogramonline.com';
//                 $toName = 'Momo Admin';
                try {
                    $model->sendTransactional($templateId, $sender, $email, $toName, $vars, $storeId);

                    if (!$mailTemplate->getSentSuccess()) {
                        // echo "Something went wrong...<br>";
                        Mage::log("Something went wrong = " . $email, null, 'jewel_oms.log');
                    } else {
                        // echo "Message sent to " . $email . "!!!<br>";
                        Mage::log("Message sent to " . $email, null, 'jewel_oms.log');
                    }
                    $translate->setTranslateInline(true);
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
        }
    }

    public function getOmsUrl()
    {
        // 5P code
        $currentUrl = Mage::helper('core/url')->getCurrentUrl();
        $url = Mage::getSingleton('core/url')->parseUrl($currentUrl);
        $path = $url->getHost();
        Mage::log(print_r($path, true), null, 'system.log');

        if (strpos($path, 'pws-services.net') !== false) {
            $omsUrl = "http://oms.monogramonline.us/hook";
            $storeID = "524339241";
        } else {
            $omsUrl = "http://oms.monogramonline.us/hook";
            $storeID = "524339241";
        }

        Mage::register('oms_array', array(
            "omsUrl" => $omsUrl,
            "storeID" => $storeID,
            "path" => $path
        ));
    }

    public function justConversionCode($incrementId)
    {
        $scriptTxt = "";
        $orderObject = Mage::getModel('sales/order')->loadByIncrementId($incrementId);

        // Mage::log(print_r($orderObject->debug(), true), null, 'jewel_oms.log', true);

        try {
            if ($orderObject->getCustomerId()) {

                $scriptTxt = '<script data-cfasync="false">window.juapp=window.juapp||function(){(window.juapp.q=window.juapp.q||[]).push(arguments)}';

                $scriptTxt .= PHP_EOL . "juapp('order','" . $orderObject->getIncrementId() . "',{total:" . $orderObject->getGrandTotal() . ",tax:" . $orderObject->getBaseTaxAmount() . ",shipping:" . $orderObject->getShippingAmount() . ",currency:'USD'});";

                $orderItems = $orderObject->getAllItems();
                $index = 1;
                foreach ($orderItems as $item) {
                    $scriptTxt .= PHP_EOL . "juapp('orderItem',";
                    $scriptTxt .= "'" . $item->getSku() . "',";
                    $scriptTxt .= "{name:'" . str_replace("'", "", $item->getName()) . "',";
                    $scriptTxt .= "quantity:" . $item->getQtyOrdered() . ",price:" . number_format(($item->getRowTotal() / $item->getQtyOrdered()), 2) . "}";
                    $scriptTxt .= ");";
                }
                $scriptTxt .= PHP_EOL . "</script>";
            } else {
                return $scriptTxt;
            }

            // Mage::log('OMS incrementId = (' . $incrementId . ') and scriptTxt = ' . $scriptTxt, null, 'jewel_oms.log');
            return $scriptTxt;
        } catch (Exception $e) {
            Mage::log('OMS incrementId = (' . $incrementId . ')  and error log  = ' . $e->getMessage(), null, 'jewel_oms.log');
        }
    }

    public function calShippingCost($orderItems, $shippingMethod )
    {
        // jewel_priorityshipping_priority

        $shipRuleName["flatrate_flatrate"]["Standard30x40"]["rate"] = [3.5];
        $shipRuleName["flatrate_flatrate"]["Standard30x40"]["skus"] = ["SFB3040", "SMB3040", "DFB3040", "DMB3040", "PLC2030-DBL", "PLC2030-SINGLE", "SQ16-SLVR-DBL", "SQ16-SLVR"];
        $shipRuleName["flatrate_flatrate"]["Standard50x60"]["rate"] = [6.91];
        $shipRuleName["flatrate_flatrate"]["Standard50x60"]["skus"] = ["SFB5060", "SMB5060", "DFB5060", "DMB5060", "SQ16-SLVR-INSERT-DBL", "SQ16-SLVR-INSERT"];


        $shipRuleName["jewel_priorityshipping_priority"]["Standard30x40"]["rate"] = [10];
        $shipRuleName["jewel_priorityshipping_priority"]["Standard30x40"]["skus"] = ["SFB3040", "SMB3040", "DFB3040", "DMB3040", "PLC2030-DBL", "PLC2030-SINGLE", "SQ16-SLVR-DBL", "SQ16-SLVR"];
        $shipRuleName["jewel_priorityshipping_priority"]["Standard50x60"]["rate"] = [10];
        $shipRuleName["jewel_priorityshipping_priority"]["Standard50x60"]["skus"] = ["SFB5060", "SMB5060", "DFB5060", "DMB5060", "SQ16-SLVR-INSERT-DBL", "SQ16-SLVR-INSERT"];
//        $shipRuleName["jewel_priorityshipping_priority"]["pillowX3.5"]["rate"] = [3.5];
//        $shipRuleName["jewel_priorityshipping_priority"]["pillowX3.5"]["skus"] = ["PLC2030-DBL", "PLC2030-SINGLE", "SQ16-SLVR-DBL", "SQ16-SLVR"];
//        $shipRuleName["jewel_priorityshipping_priority"]["pillowX6.91"]["rate"] = [6.91];
//        $shipRuleName["jewel_priorityshipping_priority"]["pillowX6.91"]["skus"] = ["SQ16-SLVR-INSERT-DBL", "SQ16-SLVR-INSERT"];


        $shipRuleName["jewel_expressshipping_express"]["Standard30x40"]["rate"] = [22];
        $shipRuleName["jewel_expressshipping_express"]["Standard30x40"]["skus"] = ["SFB3040", "SMB3040", "DFB3040", "DMB3040", "PLC2030-DBL", "PLC2030-SINGLE", "SQ16-SLVR-DBL", "SQ16-SLVR"];
        $shipRuleName["jewel_expressshipping_express"]["Standard50x60"]["rate"] = [22];
        $shipRuleName["jewel_expressshipping_express"]["Standard50x60"]["skus"] = ["SFB5060", "SMB5060", "DFB5060", "DMB5060", "SQ16-SLVR-INSERT-DBL", "SQ16-SLVR-INSERT"];
//        $shipRuleName["jewel_expressshipping_express"]["pillowX3.5"]["rate"] = [3.5];
//        $shipRuleName["jewel_expressshipping_express"]["pillowX3.5"]["skus"] = ["PLC2030-DBL", "PLC2030-SINGLE", "SQ16-SLVR-DBL", "SQ16-SLVR"];
//        $shipRuleName["jewel_expressshipping_express"]["pillowX6.91"]["rate"] = [6.91];
//        $shipRuleName["jewel_expressshipping_express"]["pillowX6.91"]["skus"] = ["SQ16-SLVR-INSERT-DBL", "SQ16-SLVR-INSERT"];


        $shippingCost = 0;
        foreach ($orderItems as $item) {

            foreach ($shipRuleName[$shippingMethod] as $keyRuleName => $ruleValue){

                foreach ($ruleValue as $keyRateSku => $valRateSku){

                    if (in_array($item->getSku(), $valRateSku))
                    {
                        $shippingCost += (int) $item->getQtyOrdered() * $ruleValue["rate"][0];
                    }
                }
            }
        }

        return $shippingCost;
    }



    public function jdbg($label, $obj)
    {

        $logStr = "JDeliveryDate -- {$label}: ";
        switch (gettype($obj)) {
            case 'boolean':
                if($obj){
                    $logStr .= "(bool) -> TRUE";
                }else{
                    $logStr .= "(bool) -> FALSE";
                }
                break;
            case 'integer':
            case 'double':
            case 'string':
                $logStr .= "(" . gettype($obj) . ") -> {$obj}";
                break;
            case 'array':
                $logStr .= "(array) -> " . print_r($obj, true);
                break;
            case 'object':
                try {
                    if (method_exists($obj, 'debug')) {
                        $logStr .= "(" . get_class($obj) . ") -> " . print_r($obj->debug(), true);
                    } else {
                        $logStr .= "Don't know how to log object of class " . get_class($obj);
                    }
                } catch (Exception $e) {
                    $logStr .= "Don't know how to log object of class " . get_class($obj);
                }
                break;
            case 'NULL':
                $logStr .= "NULL";
                break;
            default:
                $logStr .= "Don't know how to log type " . gettype($obj);
        }

        Mage::log($logStr, null, 'jdebug.log');
    }


//$order = Mage::getModel('sales/order')->load($incrementId, 'increment_id');
//$shippingprice = 30;//custom shipping price
//$order->setShippingAmount($shippingprice);
//$order->setBaseShippingAmount($shippingprice);
//$order->setGrandTotal($order->getGrandTotal() + $shippingprice); //adding shipping price to grand total
//$order->save();

}
