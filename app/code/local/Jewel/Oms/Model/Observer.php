<?php
/* We will redirect to the checkout/cart for exemple */
class Jewel_Oms_Model_Observer extends Varien_Event_Observer {

	public function sendOrderAfterSuccessAction($observer) {
		$orderIds = $observer->getOrderIds();
		$apiHelper = Mage::helper('oms_api/oms');
		
		$oms= $apiHelper->getOmsUrl();
		
		if (Mage::registry('oms_array')) {
		    $oms = Mage::registry('oms_array');
		    Mage::unregister('oms_array');
		    
		    $apiHelper->pushPurchaseToOms($orderIds, $oms['storeID'], $oms['path'], $oms['omsUrl']);
		} else {
		    Mage::log('Order_ID = (' . $orderId . ') No 6p Store found. ', null, 'jewel_oms.log');
		}
		
	}

	public function addButtons($observer)
	{
	    $block = $observer->getBlock();
	    
	    if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
	        $message = Mage::helper('core')->__('Are you sure you want to Push this order 6p ?');
	        $block->addButton('oms_button', array(
	            'label'     => Mage::helper('core')->__('Push to 6p'),
	            'onclick'   => "confirmSetLocation('{$message}', '{$block->getUrl('admin_pushorder/adminhtml_pushorderbackend/rto')}')",
	                ));
	    }
	}
}
?>
