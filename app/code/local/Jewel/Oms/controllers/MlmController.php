<?php
/**
 * Jewel_Party extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Jewel
 * @package        Jewel_Party
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Event front contrller
 *
 * @category Jewel
 * @package Jewel_Party
 * @author Jewel
 */
class Jewel_Oms_MlmController extends Mage_Core_Controller_Front_Action {
	
	/**
	 * default action
	 * Call self function
	 *
	 * @access public
	 * @return void
	 * @author Jewel
	 *         http://www.monogramathome.com/ioss/mlm/ambassador/email/tarikuli@gmail.com
	 */
    
	public function ambassadorAction() {
		$cusEmail = ( string ) $this->getRequest ()->getParam ( 'email' );
		$result = $this->getCustomerInfoByEmail ( $cusEmail );
		echo "<pre>";
		echo json_encode ( $result );
		echo "</pre>";
	}

	public function sendbuklAction() {
	    #  background-table
	    //  https://www.monogramathome.com/ioss/mlm/sendbukl/
	    echo "XXXXXXXXXXXXXx";
	    
	    
	    
	    $resource = Mage::getSingleton('core/resource');
	    $apiHelper = Mage::helper('oms_api/oms');
	    $readConnection = $resource->getConnection('core_read');
	    $writeConnection = $resource->getConnection('core_write');
	    $query = 'SELECT * FROM `email_list`';
	    
	    #$results = $readConnection->fetchAll($query);
	    $results = [];
	    
	    foreach ($results as $result){
	        set_time_limit(0);
	        $result['full_name'];
	        $result['coupon_code'];
	        
	        $vars = array(
	            'full_name' => $result['full_name'],
	            'coupon_code' => $result['coupon_code'],
	        );

	        $toMail = [
	            $result['email_to'],
	        ];
	        
	        $toName = $result['full_name'];
	        
	        
// 	        echo "<pre>";
// 	        print_r($vars);
// 	        print_r($toMail);
// 	        echo $toName;
// 	        echo "</pre>";
	        
	        $templateId = 46;
	        $apiHelper->sendNotification($toMail, $vars, $templateId, $toName);
	    }
	    
// 	    echo "<pre>";
// 	    print_r($results); 
// 	    echo "</pre>";
// // 	    var_dump($results);
	    
// 	    $toMail = [
// 	        "jewel@monogramonline.com",
// // 	        "shlomi@monogramonline.com",
// 	    ];
	    
// // 	    $vars = array(
// // 	        'full_name' => "Shlomi Matalon",
// // 	        'coupon_code' => "25offKX9AS4RYWX1C",
// // 	    );
// 	    $toName = "Shlomi Matalon";
	    
// // 	    $templateId = 46;
// //         $apiHelper->sendNotification($toMail, $vars, $templateId, $toName);
	    
	}
	
	private function getCustomerInfoByEmail($cusEmail) {
		try {
			
			$customerObject = Mage::getModel ( 'customer/customer' )->loadByEmail ( trim ( $cusEmail ) );
			
			if (empty ( $customerObject->getDefaultBillingAddress () )) {
				
				return array (
						'null' => "Ambassador email  " . $cusEmail . " not found." 
				);
			}
			
			$billing = $customerObject->getDefaultBillingAddress ();
		} catch ( Exception $e ) {
			Mage::logException ( $e );
			echo $e->getMessage ();
		}
		return $billing->toArray ();
	}
}

