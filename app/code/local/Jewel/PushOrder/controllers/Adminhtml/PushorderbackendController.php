<?php

class Jewel_PushOrder_Adminhtml_PushorderbackendController extends Mage_Adminhtml_Controller_Action
{

    protected function _isAllowed()
    {
        // return Mage::getSingleton('admin/session')->isAllowed('pushorder/pushorderbackend');
        return true;
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->_title($this->__("6P Order Push"));
        $this->renderLayout();
    }

    public function rtoAction()
    {

        if ($order = $this->_initOrder()) {
            try {
                $apiHelper = Mage::helper('oms_api/oms');
                $apiHelper->getOmsUrl();

                if (Mage::registry('oms_array')) {
                    $oms = Mage::registry('oms_array');
                    Mage::unregister('oms_array');
                    
                    $apiHelper->pushPurchaseToOms(array(
                        $order->getId()
                    ), $oms['storeID'], $oms['path'], $oms['omsUrl']);
                    $this->_getSession()->addSuccess($this->__('The order Pushed to ' . $oms['omsUrl']));
                } else {
                    $this->_getSession()->addSuccess($this->__('No 6p Store found.'));
                }
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('The order was not put on hold.'));
            }

            $this->_redirect('*/../pwsadmin/sales_order/view', array(
                'order_id' => $order->getId()
            ));

        } else {
            // Error
            $id = $this->getRequest()->getParam('order_id');
            $this->_getSession()->addError($this->__('This order id ' . $id . ' no longer exists.'));
            $this->_redirect('*/../pwsadmin/sales_order/view', array(
                'order_id' => $id
            ));
        }
    }

    /**
     * Initialize order model instance
     *
     * @return Mage_Sales_Model_Order || false
     */
    protected function _initOrder()
    {
        $id = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($id);
        
        if (! $order->getId()) {
            $this->_getSession()->addError($this->__('This order id ' . $id . ' no longer exists.'));
            $this->_redirect('*/../admin/sales_order/view', array(
                'order_id' => $id
            ));
            return false;
        }
        Mage::register('sales_order', $order);
        Mage::register('current_order', $order);
        return $order;
    }
}