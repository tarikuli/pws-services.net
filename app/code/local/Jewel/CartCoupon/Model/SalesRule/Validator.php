<?php

class Jewel_CartCoupon_Model_SalesRule_Validator extends Mage_SalesRule_Model_Validator
{
    /**
     * Add rule discount description label to address object
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @param   Mage_SalesRule_Model_Rule $rule
     * @return  Mage_SalesRule_Model_Validator
     */
    protected function _addDiscountDescription($address, $rule)
    {
        $description = $address->getDiscountDescriptionArray();
        $ruleLabel = $rule->getStoreLabel($address->getQuote()->getStore());
        // $label = '';
        // if ($ruleLabel) {
        //     $label = $ruleLabel;
        // } else if (strlen($address->getCouponCode())) {
        //     $label = $address->getCouponCode();
        // }

        $label = $address->getCouponCode();

        if (strlen($label)) {
            $description[$rule->getId()] = $label;
        }

        $address->setDiscountDescriptionArray($description);

        return $this;
    }
}