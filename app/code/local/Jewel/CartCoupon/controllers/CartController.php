<?php
class Jewel_CartCoupon_CartController extends Mage_Core_Controller_Front_Action
{
    
    protected $_quote = null;
    
    public function couponAction()
    {
        $this->loadLayout();

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('coupon', array());
            
            $oldCouponCode = $this->getQuote()->getCouponCode();
            
            $result = array();
            
            if (isset($data['use']) && intval($data['use']) == 1 && isset($data['code']) && trim($data['code']) != "") {
                $couponCode = (string) trim($data['code']);

                $result = $this->saveCoupon($couponCode);
            } else if (strlen($oldCouponCode)) {
                $result = $this->removeCoupon();
            } else {
                $this->getQuote()
                ->getShippingAddress()
                ->setCollectShippingRates(true);
                $this->getQuote()
                ->collectTotals()
                ->save();
                $result = array(
                    'status' => 'SUCCESS',
                    'html' => $this->getCouponHtml(),
                    'cart' => $this->getLayout()->getBlock('checkout.cart')->toHtml()
                );
            }
            
            $this->_prepareDataJSON($result);
            
            if (!isset($data['use'])) {
                $this->_redirect('checkout/cart');
            }
        }
    }
    
    protected function _prepareDataJSON($response)
    {
        $this->getResponse()->setHeader('Content-type', 'application/json', true);
        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }
    
    protected function _ajaxRedirectResponse()
    {
        $this->getResponse()
        ->setHeader('HTTP/1.1', '403 Session Expired')
        ->setHeader('Login-Required', 'true')
        ->sendResponse();
        return $this;
    }
    
    public function saveCoupon($couponCode)
    {
        $result = array();
        
        try {
            $codeLength = strlen($couponCode);
            //Mage::log('data =  '.print_r($codeLength, true), null, 'system.log', true);
            $isCodeLengthValid = $codeLength && $codeLength <= Mage_Checkout_Helper_Cart::COUPON_CODE_MAX_LENGTH;
            
             //Mage::log('data =  '.print_r($isCodeLengthValid, true), null, 'system.log', true);
            ##-------------------------
            $oCoupon = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');
            $data = $oCoupon->getData();
            
            # Mage::log('data =  '.print_r($data, true), null, 'system.log', true);
            
            if (empty($data)) {
                Mage::log($couponCode . " = can't seem to find this code in our database checkout page. ".(isset($_SERVER['HTTP_X_FORWARDED_FOR'])? $_SERVER['HTTP_X_FORWARDED_FOR']: ''), null, 'jewel_coupon.log');
                $this->removeCoupon();
                $result = array(
                    'status' => 'ERROR',
                    'html' => $this->getCouponHtml(),
                    'cart' => $this->getLayout()->getBlock('checkout.cart')->toHtml(),
                    'message' => Mage::helper('jewel_cartcoupon')->__('<span" style="color: #eb340a;">Coupon code "%s" is not valid code.  Please enter a valid code.</span>', Mage::helper('core')->escapeHtml($couponCode))
                );
                
                return $result;
                
            } elseif (($data['times_used'] > 0) && ($data['is_primary'] != 1)) {
                $this->removeCoupon();
                $result = array(
                    'status' => 'ERROR',
                    'html' => $this->getCouponHtml(),
                    'cart' => $this->getLayout()->getBlock('checkout.cart')->toHtml(),
                    'message' => Mage::helper('jewel_cartcoupon')->__('<span" style="color: #eb340a;">Sorry, but coupon code "%s" has been used already in a previous order.</span>', Mage::helper('core')->escapeHtml($couponCode))
                );
                
                return $result;
                
            } elseif ((! empty($data['expiration_date'])) && (strtotime($data['expiration_date']) < strtotime(date('Y-m-d 00:00:00'))) && ($data['expiration_date'] != "0000-00-00 00:00:00")) {
                $this->removeCoupon();
                $result = array(
                    'status' => 'ERROR',
                    'html' => $this->getCouponHtml(),
                    'cart' => $this->getLayout()->getBlock('checkout.cart')->toHtml(),
                    'message' => Mage::helper('jewel_cartcoupon')->__('<span" style="color: #eb340a;">Sorry, but coupon code "%s" expired.</span>', Mage::helper('core')->escapeHtml($couponCode))
                );
                
                return $result;
            }
            ##-------------------------
            #  coupon/index/save/coupon_code/DDDD
            $this->getQuote()->getShippingAddress()->setCollectShippingRates(true);
            
            $this->getQuote()->setCouponCode($isCodeLengthValid ? $couponCode : '')
                                ->collectTotals()
                                ->save();
            

            if ($codeLength) {
                $skuFound = false;
                
                $oRule = Mage::getModel('salesrule/rule')->load($oCoupon->getRuleId());
                //                 Mage::log('salesrule/rule '.print_r($oRule->getData(), true), null, 'system.log', true);
                
                // Covert conditions_serialized to Array
                $rule_arr = unserialize($oRule->getConditionsSerialized());
                $conditions_arr = isset($rule_arr['conditions'])? $rule_arr['conditions']: []; # $rule_arr['conditions'];
                
                //                 Mage::log('conditions_arr '.print_r($conditions_arr, true), null, 'system.log', true);
                $skuArr = [];
                foreach ($conditions_arr as $con_key => $con_value) {
                    foreach ($con_value['conditions'] as $condition_key => $condition_value) {
                        
                        if ($condition_value['attribute'] == "sku") {
                            $sku_arr = explode(',', $condition_value['value']);
                            foreach ($sku_arr as $valSku){
                                $skuArr[] = $valSku;
                            }
                            
                        }
                        
                    }
                }
                
                $skuArr = array_unique($skuArr);
                 //Mage::log('skuArr '.print_r($skuArr, true), null, 'system.log', true);
                $discountTotal = 0;
                foreach ($this->getQuote()->getAllItems() as $item) {
                   //  Mage::log('skuonlyArr '.print_r($skuArr.$item->getSku(), true), null, 'system.log', true);

                    if (in_array($item->getSku(), $skuArr,true)) {
                       // Mage::log('skuonlyArr '.print_r('ddff', true), null, 'system.log', true);
                        $discountTotal += $item->getDiscountAmount();
                        
                        $skuFound = true;
                    }
                }
                
                $couponData = $oRule->getData();
                
                if(!empty($couponData['coupon_code']) && ($couponCode == $this->getQuote()->getCouponCode())){
                    
                    $message = Mage::helper('jewel_cartcoupon')->__('<span" style="color: rgb(60, 179, 113);">Congrats!  discount has been applied to your cart.</span>');
                    
                    $result = array(
                        'status' => 'SUCCESS',
                        'html' => $this->getCouponHtml(),
                        'cart' => $this->getLayout()->getBlock('checkout.cart')->toHtml(),
                        'message' => $message,
                    );
                    return $result;
                }
                
                if(($couponData['coupon_type'] == 2) && ($couponData['use_auto_generation'] == 1) && ($couponCode == $data['code'])){
                    
                    $message = "";
                    if($discountTotal > 0){
                        $message = Mage::helper('jewel_cartcoupon')->__('<span" style="color: rgb(60, 179, 113);">Congrats!! discount has been applied to your cart.</span>');
                    }
                    
                    $result = array(
                        'status' => 'SUCCESS',
                        'html' => $this->getCouponHtml(),
                        'cart' => $this->getLayout()->getBlock('checkout.cart')->toHtml(),
                        'message' => $message,
                    );
                    return $result;
                }
                
                if ($skuFound == true && $isCodeLengthValid) {
                    if ($couponCode == $this->getQuote()->getCouponCode()) {
                        Mage::getSingleton('checkout/session')->setCartCouponCode($couponCode);
                        
                        
                        if (abs($couponData['discount_amount']) > $discountTotal){
                            # Mage::log('discount_amount = '.abs($couponData['discount_amount']).' DiscountTotal = '. $discountTotal .' Total= '. (abs($couponData['discount_amount'])-$discountTotal).'Prefix'.print_r($oRule->getData(), true), null, 'system.log', true);
                            
                            $leftAmount = (abs($couponData['discount_amount'])-$discountTotal);
                            
                            $message = Mage::helper('jewel_cartcoupon')->__(
                                '<span" style="color: rgb(60, 179, 113);">Congrats! A %s discount has been applied to your cart.</span><br>
                             <span" style="color: rgb(255, 0, 0);">But WAIT, coupon value is %s and you have %s credit left on your voucher code.<br>Press "Back to Collection" to go back to your voucher collection or you can "Proceed to Checkout"</span><br>
                             <button style="margin-top: 5px;" type="button" title="BACK TO COLLECTION" class="button" value="'.$couponCode.'" onclick="return gotoCoupon();" id="back_to_collection"><span><span>Back to Collection</span></span></button>
                            ',Mage::helper('core')->currency($discountTotal, true, false), Mage::helper('core')->currency($couponData['discount_amount'], true, false), Mage::helper('core')->currency($leftAmount, true, false));
                            
                        }else{
                            $message = Mage::helper('jewel_cartcoupon')->__('<span" style="color: rgb(60, 179, 113);">Congrats! A %s discount has been applied to your cart.</span>', Mage::helper('core')->currency($discountTotal, true, false));
                        }
                        
                        
                        $result = array(
                            'status' => 'SUCCESS',
                            'html' => $this->getCouponHtml(),
                            'cart' => $this->getLayout()->getBlock('checkout.cart')->toHtml(),
                            'message' => $message,
                        );
                            
                    } else {
                        Mage::log($couponCode . " not found.    ".(isset($_SERVER['HTTP_X_FORWARDED_FOR'])? $_SERVER['HTTP_X_FORWARDED_FOR']: ''), null, 'jewel_coupon.log');
                        $result = array(
                            'status' => 'ERROR',
                            'html' => $this->getCouponHtml(),
                            'cart' => $this->getLayout()->getBlock('checkout.cart')->toHtml(),
                            'message' => Mage::helper('jewel_cartcoupon')->__('<span" style="color: #eb340a;">Coupon rule "%s" is not Valid.</span>', Mage::helper('core')->escapeHtml($couponCode))
                        );
                    }
                } else {
                    $result = array(
                        'status' => 'ERROR',
                        'html' => $this->getCouponHtml(),
                        'cart' => $this->getLayout()->getBlock('checkout.cart')->toHtml(),
                        'message' => Mage::helper('jewel_cartcoupon')->__('<span" style="color: #eb340a;">"Coupon code "%s" cannot be applied at this moment. Discount condition is not valid. Please verify items in your basket, cart total amount or name of voucher/discount code"</span>', Mage::helper('core')->escapeHtml($couponCode), implode(",", $skuArr))
                    );
                    // Mage::helper('jewel_cartcoupon')->__('<span" style="color: #eb340a;">Coupon code "%s" only applies to specific store skus (%s) . None of these items are in your shopping basket. Please verify your voucher code.</span>', Mage::helper('core')->escapeHtml($couponCode), implode(",", $skuArr))
                }
            } else {
                Mage::getSingleton('checkout/session')->unsCartCouponCode();
                $result = array(
                    'status' => 'SUCCESS',
                    'html' => $this->getCouponHtml(),
                    'cart' => $this->getLayout()->getBlock('checkout.cart')->toHtml()
                );
            }
        } catch (Mage_Core_Exception $e) {
            Mage::log($couponCode . " not found1.   ".(isset($_SERVER['HTTP_X_FORWARDED_FOR'])? $_SERVER['HTTP_X_FORWARDED_FOR']: '')." Caught exception: ".$e->getMessage(), null, 'jewel_coupon.log');
            $result = array(
                'status' => 'ERROR',
                'html' => $this->getCouponHtml(),
                'cart' => $this->getLayout()->getBlock('checkout.cart')->toHtml(),
                'message' => '<span" style="color: #eb340a;">' . $e->getMessage() . '</span>'
            );
        } catch (Exception $e) {
            Mage::log($couponCode . " not found2.   ".(isset($_SERVER['HTTP_X_FORWARDED_FOR'])? $_SERVER['HTTP_X_FORWARDED_FOR']: '')." Caught exception: ".$e->getMessage(), null, 'jewel_coupon.log');
            $result = array(
                'status' => 'ERROR',
                'html' => $this->getCouponHtml(),
                'cart' => $this->getLayout()->getBlock('checkout.cart')->toHtml(),
                'message' => Mage::helper('jewel_cartcoupon')->__('<span" style="color: #eb340a;">Cannot apply the coupon code.</span>')
            );
        }
        
        return $result;
    }
    
    
    public function removeCoupon()
    {
        $quote = $this->getQuote();
        $quote->setCouponCode('');
        $quote->collectTotals()->save();
    }
    
    public function removecoupAction()
    {
        if ($this->getRequest()->isPost()) {
            $this->loadLayout();
            $quote = $this->getQuote();
            $quote->setCouponCode('');
            $quote->collectTotals()->save();
            
            $result = array(
                'status' => 'SUCCESS',
                'html' => $this->getCouponHtml(),
                'cart' => $this->getLayout()->getBlock('checkout.cart')->toHtml()
            );
            
            $this->_prepareDataJSON($result);
        }else{
            $this->_redirect('checkout/cart');
        }
    }
    
    public function getQuote()
    {
        if ($this->_quote === null) {
            return Mage::getSingleton('checkout/session')->getQuote();
        }
        return $this->_quote;
    }
    
    public function getCouponHtml()
    {
        return $this->getLayout()
        ->createBlock('checkout/cart_totals')
        ->setTemplate('checkout/cart/totals.phtml')
        ->toHtml();
    }
    
    public function ajaxProgressAction()
    {
        $result = array(
            'status' => 'SUCCESS',
            'content' => $this->getLayout()->createBlock('meanbee_extended/progress')->setTemplate('meanbee_extended/progress.phtml')->toHtml()
        );
        $this->_prepareDataJSON($result);
    }
}
