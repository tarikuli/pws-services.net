<?php

class Jewel_CartCoupon_Block_Cart_Coupon extends Mage_Checkout_Block_Cart_Abstract
{
    public function getCouponCode()
    {
        return $this->getQuote()->getCouponCode();
    }

    /**
     * Return "discount" form action url
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->getUrl('jewel_cartcoupon/cart/coupon', array('_secure' => $this->_isSecure()));
    }

    public function removeCoupActionUrl()
    {
        return $this->getUrl('jewel_cartcoupon/cart/removecoup', array('_secure' => $this->_isSecure()));
    }
    
    public function getDiscountMessage()
    {
        $discountTotal = 0;
        foreach ($this->getQuote()->getAllItems() as $item){
            $discountTotal += $item->getDiscountAmount();
        }

        return Mage::helper('jewel_cartcoupon')->__('<span" style="color: rgb(60, 179, 113);">Congrats! A %s discount has been applied to your cart.</span>', Mage::helper('core')->currency($discountTotal, true, false));
    }
    
    public function ajaxProgressActionUrl()
    {
        return $this->getUrl('jewel_cartcoupon/cart/ajaxProgress', array('_secure' => $this->_isSecure()));
    }
}
