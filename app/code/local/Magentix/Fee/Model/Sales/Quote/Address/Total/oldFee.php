<?php
/**
 * Created by Magentix
 * Based on Module from "Excellence Technologies" (excellencetechnologies.in)
 *
 * @category   Magentix
 * @package    Magentix_Fee
 * @author     Matthieu Vion (http://www.magentix.fr)
 * @license    This work is free software, you can redistribute it and/or modify it
 */

class Magentix_Fee_Model_Sales_Quote_Address_Total_Fee extends Mage_Sales_Model_Quote_Address_Total_Abstract
{

    protected $_code = 'tax';

    /**
     * Collect fee address amount
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @return Magentix_Fee_Model_Sales_Quote_Address_Total_Fee
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        parent::collect($address);

        $this->_setAmount(0);
        $this->_setBaseAmount(0);

        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }
           $finalCtaxPrice = '';
                $coupon_code = Mage::getSingleton('checkout/session')->getQuote()->getCouponCode();
                if(!empty($coupon_code)){
                    $oCoupon = Mage::getModel('salesrule/coupon')->load($coupon_code, 'code');
                    $oRule = Mage::getModel('salesrule/rule')->load($oCoupon->getRuleId());
                    $finalCtaxPrice  = $oRule->getData('coupon_purchase_price')*.08625;
                }
                $setPrice = 0;
                if (Mage::getSingleton('customer/session')->isLoggedIn()) {
           
                if($finalCtaxPrice){
                    $setPrice = $finalCtaxPrice;
                }
                }
                $grandTotal = $address->getGrandTotal();
                $baseGrandTotal = $address->getBaseGrandTotal();

                $totals = array_sum($address->getAllTotalAmounts());
                $baseTotals = array_sum($address->getAllBaseTotalAmounts());

                $address->setGrandTotal($grandTotal + $setPrice);
                $address->setBaseGrandTotal($baseGrandTotal + $setPrice);
                return $this;
    }


}