/*! */

function setCountdown(date, productID, productView){

    jQuery.noConflict();
    document.observe("dom:loaded", function() {
        
        if(productView !== undefined){
            var anchor_selector = jQuery(productView);
            anchor_selector.after('<div id="special_countdown_'+productID+'" class="special_countdown"></div>');
        }
        
        var selector = 'special_countdown_'+productID;
        var $clock = $(selector);
        jQuery('<div></div>').appendTo('body').countdown(date, function(event) {
            var days_count  = parseInt(event.strftime('%d'), 10);
            var weeks_count = parseInt(event.strftime('%w'), 10);
            var days = weeks_count * 7 + days_count;
            var strftime = event.strftime(
                //'<span class="ww"><strong>%w</strong> <b>weeks</b></span> <i>:</i>' +
                //'<span class="dd"><strong>%d</strong> <b>days</b></span> <i>:</i>' +
                '<span class="dd"><strong>'+days+'</strong> <b>days</b></span> <i>:</i>' +
                '<span class="hh"><strong>%H</strong> <b>hrs</b></span> <i>:</i>' +
                '<span class="mm"><strong>%M</strong> <b>min</b></span> <i>:</i>' + 
                '<span class="ss"><strong>%S</strong> <b>sec</b></span>'
                );
            $clock.update(strftime);
        });
        
    });
}