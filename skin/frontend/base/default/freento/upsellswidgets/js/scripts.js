var showFreentoUpsellsWidgetsPopup = function(uid) {
    jQuery.colorbox({
        html: jQuery('#freento-upsellswidgets-wrapper-' + uid).html(),
        closeButton: false,
        maxWidth: '100%',
        maxHeight: '100%',
        onLoad: colorboxResize
    });
};

var registerFreentoUpsellsWidgetsProcesscheckoutSelectors = function(selectors, uid) {
    
    var objects = $$(selectors);
    
    objects.each(function(object) {
        
        var attribute = object.getAttribute('href');
        var type = 'link';
        if(!attribute) {
            attribute = object.getAttribute('onclick');
            type = 'button';
        }
        
        if (attribute) {

            object.onclick = function() {
                return false;
            };
            
            Event.observe(object, 'click', function(event) {
                var cancelButton = '.freento-upsellswidgets-popup-cancel';
                switch (type) {
                    case 'button':
                        $$(cancelButton).each(function(el) {
                            el.writeAttribute('onclick', attribute);
                        });
                        break;
                    case 'link':
                        $$(cancelButton).each(function(el) {
                            $(el).writeAttribute('onclick', 'setLocation("' + attribute + '")');
                        });
                        break;
                    default:
                        break;
                }
                
                showFreentoUpsellsWidgetsPopup(uid);

                Event.stop(event);
            }.bind(this));
        }
            
    });

};

//Customize colorbox dimensions
var colorboxResize = function(resize = false) {
    var width = '100%',
        height = '100%';

    if (jQuery(window).width() > 767) {
        width = '80%';
        height = '80%';
    }

    jQuery.colorbox.settings.height = height;
    jQuery.colorbox.settings.width = width;

    //if window is resized while lightbox open
    if (resize) {
        jQuery.colorbox.resize({
            height: height,
            width: width
        });
    }
};

//In case of window being resized
jQuery(window).resize(function() {
    colorboxResize(true);
});
    